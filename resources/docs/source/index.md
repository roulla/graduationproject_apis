---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_ba35aa39474cb98cfb31829e70eb8b74 -->
## login
> Example request:

```bash
curl -X POST \
    "http://localhost/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST login`


<!-- END_ba35aa39474cb98cfb31829e70eb8b74 -->

<!-- START_d7aad7b5ac127700500280d511a3db01 -->
## register
> Example request:

```bash
curl -X POST \
    "http://localhost/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST register`


<!-- END_d7aad7b5ac127700500280d511a3db01 -->

<!-- START_568bd749946744d2753eaad6cfad5db6 -->
## logout
> Example request:

```bash
curl -X GET \
    -G "http://localhost/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET logout`


<!-- END_568bd749946744d2753eaad6cfad5db6 -->

<!-- START_efd9b66c83ba4d0c90200107b798212f -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/startup/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startup/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET startup/all`


<!-- END_efd9b66c83ba4d0c90200107b798212f -->

<!-- START_f4d2b3c761cff513892274ea9872264b -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/startup/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startup/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST startup/add`


<!-- END_f4d2b3c761cff513892274ea9872264b -->

<!-- START_838af54f673817e9676dcdf688f9d034 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/startup/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startup/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST startup/update/{id}`


<!-- END_838af54f673817e9676dcdf688f9d034 -->

<!-- START_d4b83352fb6c00a2e2ed421f3979727e -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/startup/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startup/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET startup/findById/{id}`


<!-- END_d4b83352fb6c00a2e2ed421f3979727e -->

<!-- START_5df6217365ba9cba4cc773b0511f87c9 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/startup/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startup/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST startup/delete/{id}`


<!-- END_5df6217365ba9cba4cc773b0511f87c9 -->

<!-- START_57e1819f02a346a816402cc4df16c958 -->
## startup/getMyMembers/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/startup/getMyMembers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startup/getMyMembers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET startup/getMyMembers/{id}`


<!-- END_57e1819f02a346a816402cc4df16c958 -->

<!-- START_d9e5e8467b39351bf709086f066e38fd -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/investor/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/investor/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET investor/all`


<!-- END_d9e5e8467b39351bf709086f066e38fd -->

<!-- START_0560214c53e9dca1edc5fdbbaa86400d -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/investor/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/investor/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST investor/add`


<!-- END_0560214c53e9dca1edc5fdbbaa86400d -->

<!-- START_58acc2a0c13e12c3ffff7959e1bba985 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/investor/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/investor/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET investor/findById/{id}`


<!-- END_58acc2a0c13e12c3ffff7959e1bba985 -->

<!-- START_56e73ab422ed538f52867e1982f54f0b -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/investor/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/investor/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST investor/update/{id}`


<!-- END_56e73ab422ed538f52867e1982f54f0b -->

<!-- START_d71e00f30672ca354f87ce2fe126f477 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/investor/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/investor/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST investor/delete/{id}`


<!-- END_d71e00f30672ca354f87ce2fe126f477 -->

<!-- START_b0796e101f42e5dedc51baad804c1fc5 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/startupMember/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startupMember/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET startupMember/all`


<!-- END_b0796e101f42e5dedc51baad804c1fc5 -->

<!-- START_025970e6cf8f781bd4c51dedcd7561f7 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/startupMember/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startupMember/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST startupMember/add`


<!-- END_025970e6cf8f781bd4c51dedcd7561f7 -->

<!-- START_3a92031c818d20b25fc5c1393736a498 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/startupMember/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startupMember/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET startupMember/findById/{id}`


<!-- END_3a92031c818d20b25fc5c1393736a498 -->

<!-- START_d0e2623d91636d47ecf73071d419f466 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/startupMember/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startupMember/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST startupMember/update/{id}`


<!-- END_d0e2623d91636d47ecf73071d419f466 -->

<!-- START_882eb1f889326f87061c531680f371e5 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/startupMember/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startupMember/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST startupMember/delete/{id}`


<!-- END_882eb1f889326f87061c531680f371e5 -->

<!-- START_db9bb1b15c7518f8f6de9caf64cd4738 -->
## startupMember/addMemberByStartupId/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/startupMember/addMemberByStartupId/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/startupMember/addMemberByStartupId/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST startupMember/addMemberByStartupId/{id}`


<!-- END_db9bb1b15c7518f8f6de9caf64cd4738 -->

<!-- START_8d9267bdf30aba13f4973f767229ed4b -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/shares/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/shares/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET shares/all`


<!-- END_8d9267bdf30aba13f4973f767229ed4b -->

<!-- START_f061f240af2833a911535a730c796620 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/shares/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/shares/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST shares/add`


<!-- END_f061f240af2833a911535a730c796620 -->

<!-- START_dd0ac0d3801417cefd1070c42e3df003 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/shares/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/shares/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET shares/findById/{id}`


<!-- END_dd0ac0d3801417cefd1070c42e3df003 -->

<!-- START_5f0ad41c2978345d025a715849f4f2db -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/shares/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/shares/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST shares/update/{id}`


<!-- END_5f0ad41c2978345d025a715849f4f2db -->

<!-- START_b5efb2bdfe28b9174e2e64bb30c0adf5 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/shares/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/shares/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST shares/delete/{id}`


<!-- END_b5efb2bdfe28b9174e2e64bb30c0adf5 -->

<!-- START_c77068964a63420a2ebda7f1a3445620 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/category/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/category/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET category/all`


<!-- END_c77068964a63420a2ebda7f1a3445620 -->

<!-- START_ce1185a9bb639db27187688d1c31a6cf -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/category/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/category/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST category/add`


<!-- END_ce1185a9bb639db27187688d1c31a6cf -->

<!-- START_334938d392699e0d747ad558018439da -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/category/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/category/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET category/findById/{id}`


<!-- END_334938d392699e0d747ad558018439da -->

<!-- START_5691435c6c1fbceaefa25bf65ed31248 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/category/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/category/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST category/update/{id}`


<!-- END_5691435c6c1fbceaefa25bf65ed31248 -->

<!-- START_a3b38b56c8dc722fa974190ed26cf5b2 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/category/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/category/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST category/delete/{id}`


<!-- END_a3b38b56c8dc722fa974190ed26cf5b2 -->

<!-- START_849b777d61f319ca0c73733dda15e7b2 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/frequency/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/frequency/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET frequency/all`


<!-- END_849b777d61f319ca0c73733dda15e7b2 -->

<!-- START_550cff3817ed31fe3c86867ae6c86be3 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/frequency/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/frequency/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST frequency/add`


<!-- END_550cff3817ed31fe3c86867ae6c86be3 -->

<!-- START_a8c2b9d019d84575d093af6530f8b9aa -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/frequency/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/frequency/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET frequency/findById/{id}`


<!-- END_a8c2b9d019d84575d093af6530f8b9aa -->

<!-- START_fa2d68266b7b57d5bc29fb00a1b3100c -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/frequency/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/frequency/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST frequency/update/{id}`


<!-- END_fa2d68266b7b57d5bc29fb00a1b3100c -->

<!-- START_4e8cbf644cd2641b608a692cfd67a65f -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/frequency/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/frequency/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST frequency/delete/{id}`


<!-- END_4e8cbf644cd2641b608a692cfd67a65f -->

<!-- START_1f46b47019ee29815c013bc9c47c22ce -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/kpi/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET kpi/all`


<!-- END_1f46b47019ee29815c013bc9c47c22ce -->

<!-- START_395b556762f15134dceae1362f1c67a8 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/kpi/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpi/add`


<!-- END_395b556762f15134dceae1362f1c67a8 -->

<!-- START_7980b99f1d1ba69cf2e4a75d4db9b442 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/kpi/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET kpi/findById/{id}`


<!-- END_7980b99f1d1ba69cf2e4a75d4db9b442 -->

<!-- START_334545539ee52c0304aaced496e944d7 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/kpi/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpi/update/{id}`


<!-- END_334545539ee52c0304aaced496e944d7 -->

<!-- START_048bf8c5094d9b1dbf5f0abd8c20541e -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/kpi/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpi/delete/{id}`


<!-- END_048bf8c5094d9b1dbf5f0abd8c20541e -->

<!-- START_902c3dd7505b0f390c095e492a745f7d -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/kpiData/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpiData/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET kpiData/all`


<!-- END_902c3dd7505b0f390c095e492a745f7d -->

<!-- START_4e7ef8407ddbc28c3286a19e7a8458d8 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/kpiData/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpiData/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpiData/add`


<!-- END_4e7ef8407ddbc28c3286a19e7a8458d8 -->

<!-- START_9719cff971289c0febe1fa9ff1ae739d -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/kpiData/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpiData/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET kpiData/findById/{id}`


<!-- END_9719cff971289c0febe1fa9ff1ae739d -->

<!-- START_cbab8787ecd5b9946da2c0ce3bdf2164 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/kpiData/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpiData/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpiData/update/{id}`


<!-- END_cbab8787ecd5b9946da2c0ce3bdf2164 -->

<!-- START_d6bdf21ca49b7c06d8ed9dec1eb44414 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/kpiData/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpiData/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpiData/delete/{id}`


<!-- END_d6bdf21ca49b7c06d8ed9dec1eb44414 -->

<!-- START_5523761aebe90e5f167b10ac98afc930 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/accountGroup/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/accountGroup/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET accountGroup/all`


<!-- END_5523761aebe90e5f167b10ac98afc930 -->

<!-- START_4f0deacbd8bb754ac0659a88f101f1d9 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/accountGroup/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/accountGroup/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST accountGroup/add`


<!-- END_4f0deacbd8bb754ac0659a88f101f1d9 -->

<!-- START_545c536cf759c5ee7d4931b0dc63d1ef -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/accountGroup/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/accountGroup/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET accountGroup/findById/{id}`


<!-- END_545c536cf759c5ee7d4931b0dc63d1ef -->

<!-- START_76fea5dc25b7dda96b316836e7fc06aa -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/accountGroup/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/accountGroup/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST accountGroup/update/{id}`


<!-- END_76fea5dc25b7dda96b316836e7fc06aa -->

<!-- START_f9c705a9a9e194676b51534fc7111064 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/accountGroup/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/accountGroup/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST accountGroup/delete/{id}`


<!-- END_f9c705a9a9e194676b51534fc7111064 -->

<!-- START_5db8e1fb2da37ee520dfe0226ae5cb43 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/transactionType/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transactionType/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET transactionType/all`


<!-- END_5db8e1fb2da37ee520dfe0226ae5cb43 -->

<!-- START_cf28a24e71cfcf977a8e5234d4232b04 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/transactionType/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transactionType/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST transactionType/add`


<!-- END_cf28a24e71cfcf977a8e5234d4232b04 -->

<!-- START_01a27a58b6888224bea9b2f2fc73dd81 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/transactionType/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transactionType/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET transactionType/findById/{id}`


<!-- END_01a27a58b6888224bea9b2f2fc73dd81 -->

<!-- START_8d3484f59f32f946df9a86e8d6811af9 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/transactionType/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transactionType/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST transactionType/update/{id}`


<!-- END_8d3484f59f32f946df9a86e8d6811af9 -->

<!-- START_768576d570a6b49e1a7f2befced1a4a7 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/transactionType/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transactionType/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST transactionType/delete/{id}`


<!-- END_768576d570a6b49e1a7f2befced1a4a7 -->

<!-- START_7198918ef4970dac54ab63b2b43c854a -->
## account/all/{status}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/account/all/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/account/all/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET account/all/{status}`


<!-- END_7198918ef4970dac54ab63b2b43c854a -->

<!-- START_5a0f81e70eed59070d091ecc9fcc1767 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/account/open" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/account/open"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST account/open`


<!-- END_5a0f81e70eed59070d091ecc9fcc1767 -->

<!-- START_f558d4166613529a87281943db81d3a3 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/account/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/account/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET account/findById/{id}`


<!-- END_f558d4166613529a87281943db81d3a3 -->

<!-- START_2db8f730b70bfff3f0dac1c10bd52de5 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/account/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/account/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST account/update/{id}`


<!-- END_2db8f730b70bfff3f0dac1c10bd52de5 -->

<!-- START_d8cf047caa57b090b9427185cbd2aa94 -->
## account/close/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/account/close/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/account/close/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST account/close/{id}`


<!-- END_d8cf047caa57b090b9427185cbd2aa94 -->

<!-- START_1194f8690ca20aadc076d26166e1492c -->
## account/reviewAllDebitTransactions/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/account/reviewAllDebitTransactions/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/account/reviewAllDebitTransactions/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET account/reviewAllDebitTransactions/{id}`


<!-- END_1194f8690ca20aadc076d26166e1492c -->

<!-- START_9780c9573edbac4fef435aed9fbc6f16 -->
## account/reviewAllCreditTransactions/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/account/reviewAllCreditTransactions/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/account/reviewAllCreditTransactions/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET account/reviewAllCreditTransactions/{id}`


<!-- END_9780c9573edbac4fef435aed9fbc6f16 -->

<!-- START_7fc4fa736a7d26937e234df6fa5e30de -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/transaction/all" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transaction/all"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET transaction/all`


<!-- END_7fc4fa736a7d26937e234df6fa5e30de -->

<!-- START_9bc4985353aeaf33d734250be5f13776 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/transaction/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transaction/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST transaction/add`


<!-- END_9bc4985353aeaf33d734250be5f13776 -->

<!-- START_db34345c354d291f98b434ec183d707a -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/transaction/findById/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transaction/findById/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET transaction/findById/{id}`


<!-- END_db34345c354d291f98b434ec183d707a -->

<!-- START_dc5200ea6555d65f24003d28637c4a1b -->
## kpi/getMyKPIs
> Example request:

```bash
curl -X GET \
    -G "http://localhost/kpi/getMyKPIs" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/getMyKPIs"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET kpi/getMyKPIs`


<!-- END_dc5200ea6555d65f24003d28637c4a1b -->

<!-- START_060028e436c5f54479ac887bc5586963 -->
## kpi/addMyKPI
> Example request:

```bash
curl -X POST \
    "http://localhost/kpi/addMyKPI" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/addMyKPI"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpi/addMyKPI`


<!-- END_060028e436c5f54479ac887bc5586963 -->

<!-- START_ba588df3dbf0483ba939f792da7f6eb1 -->
## kpi/updateMyKPI/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/kpi/updateMyKPI/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/updateMyKPI/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpi/updateMyKPI/{id}`


<!-- END_ba588df3dbf0483ba939f792da7f6eb1 -->

<!-- START_b6c1486f3fed275ab04c8950e21d391e -->
## kpi/deleteMyKPI/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/kpi/deleteMyKPI/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/deleteMyKPI/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST kpi/deleteMyKPI/{id}`


<!-- END_b6c1486f3fed275ab04c8950e21d391e -->

<!-- START_2e096a6db0ef39afa9d512f9c7b55552 -->
## kpi/findMyKPI/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/kpi/findMyKPI/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/kpi/findMyKPI/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET kpi/findMyKPI/{id}`


<!-- END_2e096a6db0ef39afa9d512f9c7b55552 -->


