<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TransactionType;
use Validator;
use App\Http\Controllers\Controller;

class TransactionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types=TransactionType::with('transactions')->get();
        return Response()->json(['status'=>'success','Message'=>'show all Transaction Types','data'=>$types],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        $type=new TransactionType();
        $type->name=$request->name;
        $type->description=$request->description;
        $type->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$type],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type=TransactionType::find($id);
        if (!isset($type)) {
        return Response()->json(['status'=>'error','Message'=>'this type doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$type],200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type=TransactionType::find($id);
        if (!isset($type)) {
        return Response()->json(['status'=>'error','Message'=>'this type doesnt exist!','data'=>''],400);
        }

        if (isset($request->name)) {
           $type->name=$request->name;
        }
        if (isset($request->description)) {
           $type->description=$request->description;
        }
        
        $type->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$type],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $type=TransactionType::find($id);
      if (!isset($type)) {
        return Response()->json(['status'=>'error','Message'=>'this type doesnt exist!','data'=>''],400);
        }
      $type->delete();
      return Response()->json(['status'=>'success','Message'=>'Deleted it!']
            ,200);
    }
}
