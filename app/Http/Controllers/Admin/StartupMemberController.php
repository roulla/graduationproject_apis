<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\StartupMember;
use App\Models\Startup;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Http\Controllers\Controller;
use DB;

class StartupMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members=StartupMember::all();

        return Response()->json(['status'=>'success','Message'=>'show all members','data'=>$members],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
 
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric',
            'shares' => 'required|numeric|max:100|min:0',
            'startup_id' => 'required',
            'avatar' => 'nullable'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        //validation FK
        $startup=Startup::find($request->startup_id);
        if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }

        $member=new StartupMember();
        $member->name=$request->name;
        $member->position=$request->position;
        $member->address=$request->address;
        $member->phone=$request->phone;
        $member->shares=$request->shares;
        $member->startup_id=$request->startup_id;
        $member->user_id=$request->user_id;
        if (isset($request->avatar)) {
            $member->avatar = Storage::disk('public_images')->put('members', $request->file('avatar'));
        }
         if(isset($request->user_id)){
        $userId=DB::table('role_user')->where('user_id', '=', $request->user_id)->get();
        if(!$userId->isEmpty()){
              return Response()->json(['status'=>'error','Message'=>'this userId is taken !','data'=>''],400);
        }
        DB::table('role_user')->insert(
            ['user_id' => $request->user_id, 'role_id' => '2']
        );}
        $member->save();
      
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$member]
            ,200);
    }


public function getUsres()
{
    $users=DB::table('users')->get();
    if (!isset($users)) {
        return Response()->json(['status'=>'error','Message'=>'there is no users!','data'=>''],400);
    }
    return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$users]
        ,200);
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       // $member=StartupMember::find($id);
        $member = StartupMember::where('id', $id)->get();
        if ($member->isEmpty()) {
        return Response()->json(['status'=>'error','Message'=>'this member doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$member]
            ,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member=StartupMember::find($id);
        if (!isset($member)) {
        return Response()->json(['status'=>'error','Message'=>'this member doesnt exist!','data'=>''],400);
        }
        $validator = Validator::make($request->all(), [
            'phone' => 'numeric',
            'shares' => 'numeric|max:100|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        if (isset($request->name)) {
           $member->name=$request->name;
        }
        if (isset($request->position)) {
           $member->position=$request->position;
        }
        if (isset($request->address)) {
           $member->address=$request->address;
        }
        if (isset($request->phone)) {
           $member->phone=$request->phone;
        }
        if (isset($request->shares)) {
           $member->shares=$request->shares;
        }
        if (isset($request->startup_id)) {
            //validation FK
            $startup=Startup::find($request->startup_id);
            if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
           $member->startup_id=$request->startup_id;
        }
        if (isset($request->avatar)) {
            $member->avatar = Storage::disk('public_images')->put('members', $request->file('avatar'));
        }
         if(isset($request->user_id)){
        $userId=DB::table('role_user')->where('user_id', '=', $request->user_id)->get();
        if(!$userId->isEmpty()){
              return Response()->json(['status'=>'error','Message'=>'this userId is taken !','data'=>''],400);
        }
        DB::table('role_user')->insert(
            ['user_id' => $request->user_id, 'role_id' => '2']
        );}
        $member->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$member]
            ,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $member=StartupMember::find($id);
      if (!isset($member)) {
        return Response()->json(['status'=>'error','Message'=>'this member doesnt exist!','data'=>''],400);
        }
        DB::table('role_user')->where('user_id',$member['user_id'])->delete();
      $member->delete();
      return Response()->json(['status'=>'success','Message'=>'Deleted it!']
            ,200);
    }


    ///add member for specific startup
    public function addMemberByStartupId(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric',
            'shares' => 'required|numeric|max:100|min:0',
            'avatar' => 'nullable'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        //validation FK
        $startup=Startup::find($id);
        if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }

        $member=new StartupMember();
        $member->name=$request->name;
        $member->position=$request->position;
        $member->address=$request->address;
        $member->phone=$request->phone;
        $member->shares=$request->shares;
        $member->startup_id=$id;
        $member->user_id=$request->user_id;
        if (isset($request->avatar)) {
            $member->avatar = Storage::disk('public_images')->put('members', $request->file('avatar'));
        }
        if(isset($request->user_id)){
        $userId=DB::table('role_user')->where('user_id', '=', $request->user_id)->get();
        if(!$userId->isEmpty()){
              return Response()->json(['status'=>'error','Message'=>'this userId is taken !','data'=>''],400);
        }
        DB::table('role_user')->insert(
            ['user_id' => $request->user_id, 'role_id' => '2']
        );}
        $member->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$member]
            ,200);
    }

     public function getMemberByStartupId($id)
    {
        $startup=Startup::find($id);
        if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
        $member = StartupMember::where('startup_id', $id)->get();
        if ($member->isEmpty()) {
        return Response()->json(['status'=>'error','Message'=>'this member doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$member]
            ,200);

    }

    /////search
    public function search($name)
    {
        $myStartup = Startup::whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->pluck('id');
        if ($myStartup->isEmpty()) {
            $myStartup='';
        }
        $member = StartupMember::where('startup_id',$myStartup)
            ->orwhereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`position`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`address`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`phone`) like ?', ['%'.strtolower($name).'%'])
            ->get();
        if (!isset($member)) {
            return Response()->json(['status'=>'error','Message'=>'this member doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$member]
            ,200);
    }
}
