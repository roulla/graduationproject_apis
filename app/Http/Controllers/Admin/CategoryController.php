<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Startup;
use App\Models\Frequency;
use Validator;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::with('kpis')->get();
        return Response()->json(['status'=>'success','Message'=>'show all Categories','data'=>$categories],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        $category=new Category();
        $category->name=$request->name;
        $category->description=$request->description;
        $category->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$category]
            ,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=Category::find($id);
        if (!isset($category)) {
        return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$category]
            ,200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category=Category::find($id);
        if (!isset($category)) {
        return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }

        if (isset($request->name)) {
           $category->name=$request->name;
        }
        if (isset($request->description)) {
           $category->description=$request->description;
        }

        $category->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$category]
            ,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $category=Category::find($id);
      if (!isset($category)) {
        return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }

      $category->delete();
      return Response()->json(['status'=>'success','Message'=>'Deleted it!']
            ,200);
    }
    ////search
    public function search($name)
    {
        $boo=false;
        $myStartup = Startup::whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->pluck('id');
        if ($myStartup->isEmpty()) {
            $myStartup='';
        }
        $myfrequency = Frequency::whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->pluck('id');
        if ($myfrequency->isEmpty()) {
            $myfrequency='';
        }
        $kpi = Category::whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereHas('kpis', function ($query) use($name,$myStartup,$myfrequency) {
                $query->whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`format`) like ?', ['%'.strtolower($name).'%'])->
                orwhere('startup_id',$myStartup)->
                orwhere('frequency_id',$myfrequency)
                ;
            })
            ->with(['kpis' =>function ($query) use($name,$myStartup,$myfrequency) {
                $query->whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`format`) like ?', ['%'.strtolower($name).'%'])->
                orwhere('startup_id',$myStartup)->
                orwhere('frequency_id',$myfrequency)
                ;
            }])->get();
        foreach($kpi as $v){
            if($v->kpis->isEmpty()){
                $boo=true;
            }}
        if($boo){
            $kpi = Category::whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])
                ->orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])
                ->orwhereHas('kpis', function ($query) use($name,$myStartup,$myfrequency) {
                    $query->whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->
                    orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])->
                    orwhereRaw('LOWER(`format`) like ?', ['%'.strtolower($name).'%'])->
                    orwhere('startup_id',$myStartup)->
                    orwhere('frequency_id',$myfrequency);
                })
                ->with('kpis')->get();
        }
        if (!isset($kpi)) {
            return Response()->json(['status'=>'error','Message'=>'no result!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$kpi]
            ,200);
    }

}
