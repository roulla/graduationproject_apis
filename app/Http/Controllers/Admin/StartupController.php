<?php

namespace App\Http\Controllers\Admin;

use App\Models\Account;
use App\Models\AccountGroup;
use Illuminate\Http\Request;
use App\Models\Startup;
use Illuminate\Support\Facades\Storage;
use Validator;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\RedmineProjectController;

class StartupController extends Controller
{

    const OPENED = 'opened';
    protected $project_redmine;

    public function __construct() {
        $this->project_redmine=new RedmineProjectController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //turn "test";
       $startups=Startup::with('startupMembers')->with('startupShares')->get();
       return Response()->json(['status'=>'success','Message'=>'show all startups','data'=>$startups],200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:startups,name',
            'phase'    => 'required|numeric|max:3|min:1',
            'joinDate' => 'required|date_format:Y-m-d',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'message' =>  $validator->errors() ] , 400);
        }

        $startup=new Startup();
        $startup->name=$request->name;
        $startup->phase=$request->phase;
        $startup->joinDate=$request->joinDate;
        if (isset($request->avatar)) {
            $startup->avatar = Storage::disk('public_images')->put('startups', $request->file('avatar'));
        }
        $startup->save();
        ///new account
        $account=new Account();
        $account->name=$request->name;
        $account->balance=0;
        $account->status=self::OPENED;
        $account->startup_id=$startup->id;
        $account->group_id=1;
        $account->parent_id=null;
        $account->save();
        //Redmine
        $this->project_redmine->create_forStartUp($startup->name,$startup->name,0);


        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$startup]
            ,200);

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $startup=Startup::find($id);
        if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$startup]
            ,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'phase'    => 'numeric|max:3|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'message' =>  $validator->errors() ] , 400);
        }
        $startup=Startup::find($id);

        if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!',
            'data'=>''],400);
        }
        $old_name=$startup->name;

        /*if (isset($request->name)) {
           $startup->name=$request->name;
           $redmine_project_id=$this->project_redmine->getIdByName($old_name);
            $this->project_redmine->update($redmine_project_id,$startup->name);
        }*/
        if (isset($request->phase)) {
           $startup->phase=$request->phase;
        }
        if (isset($request->joinDate)) {
           $startup->joinDate=$request->joinDate;
        }
        if (isset($request->avatar)) {
            $startup->avatar = Storage::disk('public_images')->put('startups', $request->file('avatar'));
        }
        $startup->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$startup]
            ,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $startup=Startup::find($id);
      if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
      $redmine_project_id=$this->project_redmine->getIdByName($startup->name);
      $startup->delete();
      $this->project_redmine->delete($redmine_project_id);

      return Response()->json(['status'=>'success','Message'=>'Deleted it!'],200);
    }
    public function getMyMembers($id)
    {
      $startup=Startup::with('startupMembers')->where('id',$id)->get();
      // if (!isset($startup)) {
      //   return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
      //   }
      return Response()->json(['status'=>'success','Message'=>'Retrieve all members for specific startup!','data'=>$startup],200);

    }
    ///////search
    public function search($name)
    {   $boo=false;
        $startup = Startup::whereRaw('LOWER( name ) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER( phase ) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER( joinDate ) like ?', ['%'.strtolower($name).'%'])
            ->orwhereHas('startupMembers', function ($query) use($name) {
                $query->whereRaw('LOWER( name ) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER( position ) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER( address ) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER( phone ) like ?', ['%'.strtolower($name).'%']);
            })
            ->with(['startupMembers' => function ($query) use($name) {
                $query->whereRaw('LOWER( name ) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER( position ) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER( address ) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER( phone ) like ?', ['%'.strtolower($name).'%']);
            }])->with('startupShares')->get();
        foreach($startup as $v){
            if($v->startupMembers->isEmpty()){
                $boo=true;
            }}
        if($boo){
            $startup = Startup::whereRaw('LOWER( name ) like ?', ['%'.strtolower($name).'%'])
                ->orwhereRaw('LOWER( phase ) like ?', ['%'.strtolower($name).'%'])
                ->orwhereRaw('LOWER( joinDate ) like ?', ['%'.strtolower($name).'%'])
                ->orwhereHas('startupMembers', function ($query) use($name) {
                    $query->whereRaw('LOWER( name ) like ?', ['%'.strtolower($name).'%'])->
                    orwhereRaw('LOWER( position ) like ?', ['%'.strtolower($name).'%'])->
                    orwhereRaw('LOWER( address ) like ?', ['%'.strtolower($name).'%'])->
                    orwhereRaw('LOWER( phone ) like ?', ['%'.strtolower($name).'%']);
                })
                ->with('startupMembers')->with('startupShares')->get();
        }
        if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'no result!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$startup]
            ,200);
    }
}
