<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Account;
use App\Models\TransactionType;
use App\Models\Startup;
use Validator;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions=Transaction::all();
        return Response()->json(['status'=>'success','Message'=>'show all Transactions','data'=>$transactions],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
{
    $newBalance=0;
    $validator = Validator::make($request->all(), [
        'credit_id' => 'required',
        'debit_id' => 'required',
        'type_id' => 'required',
        'description' => 'required',
        'amount'    => 'required|numeric|min:1',
        'date' => 'required',
    ]);
    if ($validator->fails()) {
        return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
    }

    //Validation FK
    $creditAccount= Account::find($request->credit_id);
    if (!isset($creditAccount)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
    }

    $debitAccount = Account::find($request->debit_id);
    if (!isset($debitAccount)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
    }

    $type = TransactionType::find($request->type_id);
    if (!isset($type)) {
        return Response()->json(['status'=>'error','Message'=>'this type doesnt exist!','data'=>''],400);
    }

    //check have money
    $amount = $request->amount;
   // if($debitAccount->parent_id == null) {
        if ($amount > $creditAccount->balance) {
            return Response()->json(['status' => 'failed', 'Message' => 'No money enough']
                , 400);
        }
        $newBalance = $debitAccount->balance + $amount;
   // }
    ///check not closed account
    $credit = Account::where('id',$request->credit_id)->pluck('status');
    $debit = Account::where('id',$request->debit_id)->pluck('status');
    if($credit[0]==='closed'or $debit[0]==='closed')
    {
        return Response()->json(['status'=>'failed','Message'=>'you use closed account']
            ,400);
    }

    else
    {
        //success Add
        $transaction=new Transaction();
        $transaction->credit_id=$request->credit_id;
        $transaction->debit_id=$request->debit_id;
        $transaction->type_id=$request->type_id;
        $transaction->description=$request->description;
        $transaction->amount=$request->amount;
        $transaction->date=$request->date;


        $newBalance2= $creditAccount->balance - $amount;
        $debitAccount->balance = $newBalance;
        $creditAccount->balance = $newBalance2;
        $creditAccount->save();
        $debitAccount->save();
        $transaction->save();

        return Response()->json(['status'=>'success','Message'=>'Done create Transaction','data'=>$transaction]
            ,200);
    }

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction=Transaction::find($id);
        if (!isset($transaction)) {
            return Response()->json(['status'=>'error','Message'=>'this transaction doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$transaction]
            ,200);
    }
    /////search by startup name
    public function search($name)
    {
        $myStartup = Startup::whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->pluck('id');
        if ($myStartup->isEmpty()) {
            $myStartup='';
        }
        $transaction = Transaction::where('credit_id',$myStartup)->orwhere('debit_id',$myStartup)
            ->orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`date`) like ?', ['%'.strtolower($name).'%'])
            ->get();
        if (!isset($transaction)) {
            return Response()->json(['status'=>'error','Message'=>'no result!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$transaction]
            ,200);
    }
    public function GetAccounttrans($id)
    {
        $transaction = Transaction::where('credit_id',$id)->orwhere('debit_id',$id)->orderby('date')->get()
            ->groupBy(function($item) {
                return Carbon::createFromFormat('Y-m-d', $item->date )->format('Y');
            } )->map(function ($items) {
        return $items->groupBy(function($item2) {
            return Carbon::createFromFormat('Y-m-d', $item2->date )->format('M');
        });});

       if ($transaction->isEmpty()) {
            return Response()->json(['status'=>'error','Message'=>'no result!','data'=>''],400);
        }
$data=[];
       $temp=[];
       $temp2=[];
foreach($transaction as $key=> $item){
    foreach ($item as $key2=>$value){
        $index=0;

        foreach ($value as $element){
     $credit_name = Account::where('id',$element->credit_id)->pluck('name');
        $debit_name = Account::where('id',$element->debit_id)->pluck('name');
        $type_name=TransactionType::where('id',$element->type_id)->pluck('name');
   $value[$index]["credit_name"]= $credit_name[0];
    $value[$index]["debit_name"]= $debit_name[0];
            $value[$index]["type_name"]= $type_name[0];
    $index++;
           }

        $temp2['month_date']=$key2;
        $temp2['month_transactions']=$value;
        array_push($temp,$temp2);

    }
    $temp3['year']=$key;
    $temp3['months']=$temp;
    array_push( $data,$temp3);
    $temp=[];
}


        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$data]
            ,200);
    }
}
