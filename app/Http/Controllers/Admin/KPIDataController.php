<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\KPI_Data;
use App\Models\Frequency;
use Carbon\Carbon;
use App\Models\KPI;
use Validator;
use App\Http\Controllers\Controller;

class KPIDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=KPI_Data::orderBy('date', 'asc')->get();
        return Response()->json(['status'=>'success','Message'=>'show all data of kpi','data'=>$data],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

 public function getTotal($kpiId)
    {
        $kpi=KPI::findOrFail($kpiId);
        $data=$kpi->datas;
        $total=0;
        //$data=KPI_Data::with('datas')->get('actual');
         foreach ($data as $v) {
         $total+=$v->actual;  
        }
        return Response()->json(['status'=>'success','Message'=>'total value','data'=>$total],200);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'kpi_id' => 'required',
            'actual' => 'required|numeric|min:0',
            'date' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        //validation FK
        $kpi=KPI::find($request->kpi_id);
        if (!isset($kpi)) {
            return Response()->json(['status'=>'error','Message'=>'this kpi doesnt exist!','data'=>''],400);
        }
        $frequency=Frequency::where('id','=',$kpi->frequency_id)->pluck('name');
        //add success
       // $Date_now = Carbon::today()->format('Y-m-d');
        $Date_now=date('Y-m-d', strtotime($request->date));
        $last_inserted_data=KPI_Data::where('kpi_id','=',$request->kpi_id)->orderBy('id', 'DESC')->get()->first();
        if (isset($last_inserted_data)) {
            $last_date=Carbon::parse($last_inserted_data->date)->format('Y-m-d');

            if($frequency[0] == 'daily')
            {
                $next_day= date('Y-m-d', strtotime($last_date. ' + 1 days'));
                if($Date_now<$next_day){
                    return Response()->json(['status'=>'error','Message'=>'the day is not over you have to wait to '.$next_day,'data'=>''],400);
                }
            }

            elseif($frequency[0] == 'weekly')
            {
                $next_week= date('Y-m-d', strtotime($last_date. ' + 7 days'));
                if($Date_now<$next_week){
                    return Response()->json(['status'=>'error','Message'=>'the week is not over you have to wait to '.$next_week,'data'=>''],400);
                }
            }

            elseif($frequency[0] == 'monthly')
            {
                $next_month= date('Y-m-d', strtotime($last_date. ' + 30 days'));
                if($Date_now<$next_month){
                    return Response()->json(['status'=>'error','Message'=>'the month is not over you have to wait to '.$next_month,'data'=>''],400);
                }

            }
        }
        $data=new KPI_Data();
        $data->kpi_id=$request->kpi_id;
       /* if(!empty($last_inserted_data)){
            $data->actual=$last_inserted_data->actual+$request->actual;}*/
      //  else{
        $data->actual=$request->actual;
       // }
        $data->date=$Date_now;
        $data->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$data],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=KPI_Data::find($id);
        if (!isset($data)) {
        return Response()->json(['status'=>'error','Message'=>'this data doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$data],200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=KPI_Data::find($id);
        if (!isset($data)) {
            return Response()->json(['status'=>'error','Message'=>'this data doesnt exist!','data'=>''],400);
        }

        $validator = Validator::make($request->all(), [
            'actual' => 'numeric|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        if (isset($request->kpi_id)) {
            //validation FK
            $kpi=KPI::find($request->kpi_id);
            if (!isset($kpi)) {
                return Response()->json(['status'=>'error','Message'=>'this kpi doesnt exist!','data'=>''],400);
            }
            $data->kpi_id=$request->kpi_id;
        }
        if (isset($request->actual)) {
           /* $old_actual=$data->actual;
            $new_actual=$request->actual-$old_actual;
            $data->actual=$request->actual;
            $last_inserted_datas=KPI_Data::where('kpi_id','=',$request->kpi_id)->where('date','>',$data->date)->get();
            foreach ($last_inserted_datas as $v){
                $v->actual=$v->actual+$new_actual;
                $v->save();
            }*/
         $data->actual=$request->actual;
        }
        if (isset($request->date)) {
            return Response()->json(['status'=>'error','Message'=>'you cannot change the date!','data'=>''],400);
        }

        $data->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$data],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=KPI_Data::find($id);
        if (!isset($data)) {
        return Response()->json(['status'=>'error','Message'=>'this data doesnt exist!','data'=>''],400);
        }
        $data->delete();
        return Response()->json(['status'=>'success','Message'=>'Deleted it!'],200);
    }

    public function findByKpiId($id)
    {
        $kpi=KPI::findOrFail($id);

        //$dd=KPI::with('datas:actual')->where('id',$id)->get();

        return Response()->json(['status'=>'success','Message'=>'found it !',
            'data'=>[
            'target' => $kpi->target ,
            'kpi data' => $kpi->datas
        ]],200);
    }
}
