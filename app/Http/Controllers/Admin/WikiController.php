<?php

namespace App\Http\Controllers\Admin;

use App\Models\StartupMember;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\RedmineService;
use App\Models\Startup;
use Auth;
use Validator;
use Illuminate\Support\Facades\Storage;
use Response;
class WikiController extends Controller
{
    protected $service;
    protected $connected;

    public function __construct()
    {
        $this->service = new RedmineService();
        $this->connected = $this->service->connectToLibrary();
    }
    public function all($project_id)
    {
        $wiki=$this->connected->wiki->all($project_id);
        return Response()->json(['status' => 'success', 'Message' => 'result',
            'data'=>$wiki], 200);
    }

    public function all2()
    {
        $projects=$this->connected->project->all()['projects'];
        $res=[];
        foreach ($projects as $val){
            $wiki=$this->connected->wiki->all(strtolower($val['id']))['wiki_pages'];
            $res= array_merge($res,$wiki);
        }
        return Response()->json(['status' => 'success', 'Message' => 'result',
            'data'=>$res], 200);
    }

    public function show(Request $request,$title)
    {
        $validator = Validator::make($request->all(), [
            'project_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        return Response()->json(['status' => 'success', 'Message' => 'result',
            'data'=>[$this->connected->wiki->show($request->project_id,$title)['wiki_page']]], 200);
    }

    public function update(Request $request,$title)
    {
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'version' =>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        $this->connected->wiki->update($request->project_id,$title, [
            'text' => $request->description,
            'version' =>$request->version,
        ]);
        return Response()->json(['status' => 'success', 'Message' => 'updated it',
            'data'=>'Done'], 200);
    }

    public function delete(Request $request,$title)
    {

        $validator = Validator::make($request->all(), [
            'project_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        return Response()->json(['status' => 'success', 'Message' => 'deleted it',
            'data'=>$this->connected->wiki->remove($request->project_id, $title)], 200);
    }

    public function upload2(Request $request)
    {

          print_r(json_decode($request->filecontent));
    }
    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'title'=>'required',
            'text'=>'required',
            'filecontent'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        $file= $this->connected->wiki->show($request->project_id, $request->title);
        if(!empty($file)){
           if(empty($file['wiki_page']['attachments'])){
            $upload = json_decode($this->connected->attachment->upload($request->filecontent));
            $this->connected->wiki->create($request->project_id, $request->title, [
                'text' => $request->text,
                'comments' => $request->cooments,
                'version' =>$request->version,
                'uploads' => [
                    [
                        'token' => $upload->upload->token,
                        'filename' =>$request->filename.'.'.$request->filecontent->extension(),
                        'description' => $request->description,
                        'content_type' =>'application/'.$request->filecontent->getMimeType(),
                    ],
                ],
            ]);
        }
           else{
            return Response()->json(['status' => 'error', 'Message' => 'this folder is not empty!', 'data' => ''], 400);
        }}
       else{
           $upload = json_decode($this->connected->attachment->upload($request->filecontent));
           $this->connected->wiki->create($request->project_id, $request->title, [
               'text' => $request->text,
               'comments' => $request->cooments,
               'version' =>$request->version,
               'uploads' => [
                   [
                       'token' => $upload->upload->token,
                       'filename' =>$request->filename.'.'.$request->filecontent->extension(),
                       'description' => $request->description,
                       'content_type' =>'application/'.$request->filecontent->getMimeType(),
                   ],
               ],
           ]);
       }

        return Response()->json(['status' => 'success', 'Message' => 'upload it!','data'=>'done'], 200);

//return Response()->json(['status' => 'error', 'Message' => 'this issue doesnt exist!', 'data' => ''], 400);
    }




    public function download(Request $request,$id)
    {
        

        $file_content =[$this->connected->attachment->show($id)];
        // $contents =file_put_ contents($request->filename, $file_content);
        $url=$file_content[0]['attachment']['content_url'];
      /*  $filename = $request->filename;
        $tempImage = tempnam(sys_get_temp_dir(), $filename);
        copy($url, $tempImage);
        return response()->download($tempImage, $filename);*/
        return Response()->json(['status' => 'success', 'Message' => 'done','data'=>$url], 200);

    }

        public function downloadFromWiki(Request $request)
    {  
     $wiki=$this->connected->wiki->show($request->project_id, $request->title)['wiki_page']['attachments'][0]['content_url'];
        return Response()->json(['status' => 'success', 'Message' => 'done','data'=>['content_url'=>$wiki]], 200);
    }

}
