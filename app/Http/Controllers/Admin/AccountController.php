<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Startup;
use App\Models\AccountGroup;
use DB;
use Validator;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Global variable
    const OPENED = 'opened';
    const CLOSED = 'closed';


    public function index(Request $request)
    {
        $status = $request->status;

        $accounts = Account::where('status', $status)->get();
        return Response()->json(['status'=>'success','Message'=>'show all Accounts','data'=>$accounts],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation request
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:accounts,name',
            'balance'    => 'required|numeric|min:0',
            'startup_id' => 'required',
            'group_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' => $validator->messages()->first() ] , 400);
        }

        //validation FK
        $startup=Startup::find($request->startup_id);
        if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }

        $group=AccountGroup::find($request->group_id);
        if (!isset($group)) {
        return Response()->json(['status'=>'error','Message'=>'this group doesnt exist!','data'=>''],400);
        }
        $parent_id = Account::where(['startup_id' => $request->startup_id ,
            'status' => 'opened'])->pluck('id');
      /*  if (isset($request->parent_id)){

        $openAccount = Account::where('status', 'opened')->find($request->parent_id);
        if (!isset($openAccount)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
        }
    }*/

        //add new account
        $account=new Account();
        $account->name=$request->name;
        $account->balance=$request->balance;
        $account->status=self::OPENED;
        $account->startup_id=$request->startup_id;
        $account->group_id=$request->group_id;
        $account->parent_id=$parent_id[0];
        $account->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$account]
            ,200);
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account=Account::find($id);
        if (!isset($account)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$account],200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'balance'    => 'numeric|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        $account=Account::find($id);
        if (!isset($account)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!',
            'data'=>''],400);
        }

        if (isset($request->name)) {
           $account->name=$request->name;
        }
        if (isset($request->balance)) {
           $account->balance=$request->balance;
        }
        if (isset($request->status)) {
           $account->status=$request->status;
        }
        if (isset($request->startup_id)) {
            $startup=Startup::find($request->startup_id);
            if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
           $account->startup_id=$request->startup_id;
        }
        if (isset($request->group_id)) {
            $group=AccountGroup::find($request->group_id);
            if (!isset($group)) {
            return Response()->json(['status'=>'error','Message'=>'this group doesnt exist!','data'=>''],400);
        }
           $account->group_id=$request->group_id;
        }
        if (isset($request->parent_id)) {
            $openAccount = Account::where('status', 'opened')->find($request->parent_id);
            if (!isset($openAccount)) {
            return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
        }
           $account->parent_id=$request->parent_id;
        }

        $account->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$account],200);
    }

    public function close($id)
    {
      $account=Account::find($id);
      if (!isset($account)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!',
            'data'=>''],400);
        }

      $account->status=self::CLOSED;
      $account->save();
      return Response()->json(['status'=>'success','Message'=>'Closed it!'] ,200);
    }

    public function ReviewAllCreditTransactions($id)
    {
       $account=Account::find($id);
       if (!isset($account)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!',
            'data'=>''],400);
        }
       $transactions=$account->creditTransactions;
      // $account=Account::with('transactions')->get();
       return Response()->json(['status'=>'success','Message'=>'Review All credit Transactions On Account','data'=>$transactions],200);
    }

    public function ReviewAllDebitTransactions($id)
    {
       $account=Account::find($id);
       if (!isset($account)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!',
            'data'=>''],400);
        }
       $transactions=$account->debitTransactions;
       return Response()->json(['status'=>'success','Message'=>'Review All Debit Transactions On Account','data'=>$transactions],200);
    }

    // public function GetAllClosedAccounts()
    // {
    //  $accounts = DB::table('accounts')
    //             ->where('status','=', 'closed')
    //             ->get();
    //     return Response()->json(['status'=>'success','Message'=>'Retrieved All Closed Accounts','data'=>$accounts],200);
    // }

    // public function GetBalance($id)
    // {
    //   $account=Account::find($id);
    //   return Response()->json(['status'=>'success','Message'=>'Retrieved Balance Account','data'=>$account->balance]
    //         ,200);
    // }

    ////////////flatter

    //////////////get  accounts in groups from Startup
    public function  getMyaccountsByStartupId($id)
    {
        $startup=Startup::find($id);
        if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }

        $groupid = Account::where('startup_id','=', $id)->pluck('group_id');
        if ($groupid->isEmpty()) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt have groups!','data'=>''],400);
        }
        $groups=AccountGroup::whereIn('id', $groupid)->with(['accounts'=> function($query) use($id) {
            $query->select('*')->where('startup_id','=',$id);
        }])->get();
        return Response()->json(['status'=>'success','Message'=>'show all Account Groups','data'=>$groups],200);
    }

}
