<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\KPI;
use Validator;
use App\Models\Startup;
use App\Models\Frequency;
use App\Models\Category;
use App\Models\KPI_Data;
use Auth;
use App\Http\Controllers\Controller;

class KPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kpis=KPI::join('startups','startups.id','k_p_i_s.startup_id')
            ->join('frequencies','frequencies.id','k_p_i_s.frequency_id')
            ->join('categories','categories.id','k_p_i_s.category_id')
            ->select('k_p_i_s.*','startups.name as startup_name','frequencies.name as frequency_name','categories.name as category_name')
        ->with('datas')->get();
        return Response()->json(['status'=>'success','Message'=>'show all kpis','data'=>$kpis],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'startup_id' => 'required',
            'category_id' => 'required',
            'frequency_id' => 'required',
            'target' => 'required|numeric|min:1',
            'description' => 'required',
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        //validation FK
        $startup=Startup::find($request->startup_id);
        if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }

        $frequency=Frequency::find($request->frequency_id);
        if (!isset($frequency)) {
        return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }

        $category=Category::find($request->category_id);
        if (!isset($category)) {
        return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }

        //add Success
        $kpi=new KPI();
        $kpi->startup_id=$request->startup_id;
        $kpi->category_id=$request->category_id;
        $kpi->frequency_id=$request->frequency_id;
        $kpi->name=$request->name;
        $kpi->target=$request->target;
        $kpi->description=$request->description;
        $kpi->format='numeric';

        $kpi->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$kpi],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kpi=KPI::where('id',$id)->with(['datas'=> function($query)  {$query->orderBy('date', 'asc');}])->get();
        if (!isset($kpi)) {
        return Response()->json(['status'=>'error','Message'=>'this kpi doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$kpi],200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kpi=KPI::find($id);
        if (!isset($kpi)) {
        return Response()->json(['status'=>'error','Message'=>'this kpi doesnt exist!','data'=>''],400);
        }

        $validator = Validator::make($request->all(), [
            'target' => 'numeric|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        if (isset($request->startup_id)) {
            $startup=Startup::find($request->startup_id);
            if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
           $kpi->startup_id=$request->startup_id;
        }
        if (isset($request->category_id)) {
            $category=Category::find($request->category_id);
            if (!isset($category)) {
            return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }
           $kpi->category_id=$request->category_id;
        }
        if (isset($request->frequency_id)) {
            $frequency=Frequency::find($request->frequency_id);
            if (!isset($frequency)) {
            return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }
           $kpi->frequency_id=$request->frequency_id;
        }
        if (isset($request->name)) {
           $kpi->name=$request->name;
        }
        if (isset($request->target)) {
           $kpi->target=$request->target;
        }
        if (isset($request->description)) {
           $kpi->description=$request->description;
        }
//        if (isset($request->format)) {
//           $kpi->format=$request->format;
//        }

        $kpi->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$kpi],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kpi=KPI::find($id);
        if (!isset($kpi)) {
        return Response()->json(['status'=>'error','Message'=>'this kpi doesnt exist!','data'=>''],400);
        }
        $kpi->delete();
        return Response()->json(['status'=>'success','Message'=>'Deleted it!'],200);
    }
    ///////search
    public function search($name)
    {
        $boo=false;
        $myStartup = Startup::whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->pluck('id');
        if ($myStartup->isEmpty()) {
            $myStartup='';
        }
        $kpidata = KPI::where('startup_id', $myStartup)
            ->orwhereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`target`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`format`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereHas('datas', function ($query) use($name) {
                $query->whereRaw('LOWER(`actual`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`date`) like ?', ['%'.strtolower($name).'%']);
            })
            ->with(['datas'=>function ($query) use($name) {
                $query->whereRaw('LOWER(`actual`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`date`) like ?', ['%'.strtolower($name).'%']);
            }])
            ->get();
        foreach($kpidata as $v){
            if($v->datas->isEmpty()){
                $boo=true;
            }}
        if($boo){
            $kpidata = KPI::where('startup_id', $myStartup)
                ->orwhereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])
                ->orwhereRaw('LOWER(`target`) like ?', ['%'.strtolower($name).'%'])
                ->orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])
                ->orwhereRaw('LOWER(`format`) like ?', ['%'.strtolower($name).'%'])
                ->orwhereHas('datas', function ($query) use($name) {
                    $query->whereRaw('LOWER(`actual`) like ?', ['%'.strtolower($name).'%'])->
                    orwhereRaw('LOWER(`date`) like ?', ['%'.strtolower($name).'%']);
                })
                ->with('datas')
                ->get();
        }
        if (!isset($kpidata)) {
            return Response()->json(['status'=>'error','Message'=>'no result!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$kpidata]
            ,200);
    }

    ////flatter
    /////get category from Startup id
    public function getMycategory($id)
    {
        $startup=Startup::find($id);
        if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
        $categoryid = KPI::where('startup_id','=', $id)->pluck('category_id');
        if ($categoryid->isEmpty()) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt have categories!','data'=>''],400);
        }
       
        $categories=Category::whereIn('id', $categoryid)->with(['kpis'=> function($query) use($id) {
            $query->where('startup_id','=',$id)->join('frequencies','frequencies.id','k_p_i_s.frequency_id')->select('k_p_i_s.*','frequencies.name as frequency_name');
        }])->get();
        return Response()->json(['status'=>'success','Message'=>'show all Categories','data'=>$categories],200);

    }


    /////get frequency from Startup id
    public function getMyfrequency($id)
    {
        $startup=Startup::find($id);
        if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
        $frecuencyid = KPI::where('startup_id','=', $id)->pluck('frequency_id');
        if ($frecuencyid->isEmpty()) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt have frecuencies!','data'=>''],400);
        }
        $frequencies=Frequency::whereIn('id', $frecuencyid)->with(['kpis'=> function($query) use($id) {
            $query->select('*')->where('startup_id','=',$id);
        }])->get();


        return Response()->json(['status'=>'success','Message'=>'show all frequencies','data'=>$frequencies],200);

    }


    /////get kpi from Startup id
    public function getMykpi($id)
    {
        $startup=Startup::find($id);
        if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
        $kpies =KPI::where('startup_id','=', $id)->with('datas')->get();;
        if ($kpies->isEmpty()) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt have kpis!','data'=>''],400);
        }

        return Response()->json(['status'=>'success','Message'=>'show all kpies','data'=>$kpies],200);

    }
    /////add kpi from startup id

    public function addkpiByStartupId(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'frequency_id' => 'required',
            'target' => 'required|numeric|min:1',
            'description' => 'required',
            'name' => 'required',
            'format' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        //validation FK
        $startup=Startup::find($id);
        if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }

        $frequency=Frequency::find($request->frequency_id);
        if (!isset($frequency)) {
            return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }

        $category=Category::find($request->category_id);
        if (!isset($category)) {
            return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }

        //add Success
        $kpi=new KPI();
        $kpi->startup_id=$id;
        $kpi->category_id=$request->category_id;
        $kpi->frequency_id=$request->frequency_id;
        $kpi->name=$request->name;
        $kpi->target=$request->target;
        $kpi->description=$request->description;
        $kpi->format='numeric';

        $kpi->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$kpi],200);
    }
}
