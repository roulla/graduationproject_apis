<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Investor;
use App\Models\Startup;
use App\Models\InvestorStartup;
use Illuminate\Support\Facades\Storage;
use Validator;
use JWTAuth;
use Zizaco\Entrust\HasRole;
use Auth;
use App\Http\Controllers\Controller;

class InvestorController extends Controller
{


    /**
     * @var
     */
    protected $user;
    //protected $currentUser;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
       // $currentUser = Auth::user();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //if(Auth::user()->can(['investor-list'])){
        $investors=Investor::with('investorShares')->get();

        return Response()->json(['status'=>'success','Message'=>'Investors retrieved successfully','data'=>$investors],200);
       // }
        //return response()->json(['status' => 'error' , 'message' =>  'Unauthorize' ] , 401);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric',
            'type' => 'required',
            'avatar' => 'nullable'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        $investor=new Investor();
        $investor->name=$request->name;
        $investor->position=$request->position;
        $investor->address=$request->address;
        $investor->phone=$request->phone;
        $investor->type=$request->type;
        if (isset($request->avatar)) {
            $investor->avatar = Storage::disk('public_images')->put('investors', $request->file('avatar'));
        }

        $investor->save();
        return Response()->json(['status'=>'success','Message'=>'Investor created successfully.',
            'data'=>$investor],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $investor=Investor::find($id);
        if (!isset($investor)) {
        return Response()->json(['status'=>'error','Message'=>'this investor doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$investor]
            ,200);
    }

    ////flatter
    public function show2($id)
    {
        $investor_id=Investor::find($id);
        if (!isset($investor_id)) {
            return Response()->json(['status'=>'error','Message'=>'this investor doesnt exist!','data'=>''],400);
        }
        $investor=Investor::where('investors.id', '=', $id)->
        with(['investorShares'=> function($query)  {
            $query->select('shares','investor_id','startups.name as startupName','investor_startups.created_at as joined date to this startup','investor_startups.updated_at as update joined date to this startup'
            )->join('startups','startups.id','investor_startups.startup_id');
        }])->select('investors.name as investor name','investors.id','address','position','phone','type','investors.created_at as investor joined date',
            'investors.updated_at as investor updated_at')->get();

        if (!isset($investor)) {
            return Response()->json(['status'=>'error','Message'=>'this investor doesnt exist!','data'=>''],400);
        }

        //  $investor->push(['startupname' => $startupname]);investor_shares.id

        unset($investor[0]['id']);
        foreach($investor as $v){
            foreach($v->investorShares as $key){
                unset($key['investor_id']);
            }
        }

        //echo $investor[0]->investorShares[0];


        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$investor]
            ,200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $investor=Investor::find($id);
        if (!isset($investor)) {
        return Response()->json(['status'=>'error','Message'=>'this investor doesnt exist!','data'=>''],400);
        }

        $validator = Validator::make($request->all(), [
            'phone' => 'numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        if (isset($request->name)) {
           $investor->name=$request->name;
        }
        if (isset($request->position)) {
           $investor->position=$request->position;
        }
        if (isset($request->address)) {
           $investor->address=$request->address;
        }
        if (isset($request->phone)) {
           $investor->phone=$request->phone;
        }
        if (isset($request->type)) {
           $investor->type=$request->type;
        }
        if (isset($request->avatar)) {
            $investor->avatar = Storage::disk('public_images')->put('investors', $request->file('avatar'));
        }
        $investor->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$investor]
            ,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $investor=Investor::find($id);
        if (!isset($investor)) {
        return Response()->json(['status'=>'error','Message'=>'this investor doesnt exist!','data'=>''],400);
        }
        $investor->delete();
        return Response()->json(['status'=>'success','Message'=>'Deleted it!']
            ,200);
    }
}
