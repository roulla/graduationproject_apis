<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\Role;
use App\Models\Permission;
use App\Http\Controllers\Controller;
use DB;
class RoleController extends Controller
{

 public function all()
    {
    	$usres=DB::table('role_user')->get();
     return Response()->json(['status' => 'success', 'Message' => 'Done ', 'data' =>$usres], 200);
 }

	 public function destroy($id)
    {
     $usres=DB::table('role_user')->where('user_id',$id)->delete();
     return Response()->json(['status' => 'success', 'Message' => 'Done ', 'data' =>$usres], 200);
 }
}
