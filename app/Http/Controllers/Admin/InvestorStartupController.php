<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\InvestorStartup;
use App\Models\Startup;
use App\Models\Investor;
use Validator;
use App\Http\Controllers\Controller;

class InvestorStartupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$shares=InvestorStartup::with('investors')->with('startups')->get();

         $shares=InvestorStartup::join('startups','startups.id','investor_startups.startup_id')
              ->join('investors','investors.id','investor_startups.investor_id')
              ->select('investor_startups.id','investor_startups.shares','startups.name as startup_name','investors.name as investor_name','investor_startups.created_at','investor_startups.updated_at')
              ->get();
          //$startup_name=$shares->startups->lists('name');
         // $investor_name=$shares->investors->name;

          return Response()->json(['status'=>'success','Message'=>'show all shares','data'=>$shares],200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'startup_id' => 'required' ,
            'investor_id' => 'required' ,
            'shares' => 'required|numeric|max:100|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        //validation FK
        $startup=Startup::find($request->startup_id);
        if (!isset($startup)) {
        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }

        $investor=Investor::find($request->investor_id);
        if (!isset($investor)) {
        return Response()->json(['status'=>'error','Message'=>'this investor doesnt exist!','data'=>''],400);
        }

        //add success
        $share=new InvestorStartup();
        $share->startup_id=$request->startup_id;
        $share->investor_id=$request->investor_id;
        $share->shares=$request->shares;
        $share->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$share]
            ,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $share=InvestorStartup::find($id);
        if (!isset($share)) {
        return Response()->json(['status'=>'error','Message'=>'this share doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$share]
            ,200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $share=InvestorStartup::find($id);
        if (!isset($share)) {
        return Response()->json(['status'=>'error','Message'=>'this share doesnt exist!','data'=>''],400);
        }

        $validator = Validator::make($request->all(), [
            'shares' => 'numeric|max:100|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        if (isset($request->startup_id)) {
            $startup=Startup::find($request->startup_id);
            if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
           $share->startup_id=$request->startup_id;
        }
        if (isset($request->investor_id)) {
            $investor=Investor::find($request->investor_id);
            if (!isset($investor)) {
            return Response()->json(['status'=>'error','Message'=>'this investor doesnt exist!','data'=>''],400);
        }
           $share->investor_id=$request->investor_id;
        }
        if (isset($request->shares)) {
           $share->shares=$request->shares;
        }

        $share->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$share]
            ,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $share=InvestorStartup::find($id);
        if (!isset($share)) {
        return Response()->json(['status'=>'error','Message'=>'this share doesnt exist!','data'=>''],400);
        }
        $share->delete();
        return Response()->json(['status'=>'success','Message'=>'Deleted it!'] ,200);
    }

    /////get from Startup id
    public function getMyinvestor($id)
    {
        $startup=Startup::find($id);
        if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }
        $investorid = InvestorStartup::where('startup_id', '=', $id)->pluck('investor_id');
        if ($investorid->isEmpty()) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt have investors!','data'=>''],400);
        }
        $investor=Investor::whereIn('id', $investorid)->with(['investorShares'=>function($query) use($id) {
            $query->select('*')->where('startup_id','=',$id);
        }])->get();

        if (!isset($investor)) {
            return Response()->json(['status'=>'error','Message'=>'this investor doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$investor]
            ,200);
    }

    /////add from startup id
    public function addinvestorByStartupId(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric',
            'type' => 'required',
            'shares' => 'required|numeric|max:100|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        //validation FK
        $startup=Startup::find($id);
        if (!isset($startup)) {
            return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
        }

        $investor=new Investor();
        $investor->name=$request->name;
        $investor->position=$request->position;
        $investor->address=$request->address;
        $investor->phone=$request->phone;
        $investor->type=$request->type;
        $investor->save();

        $share=new InvestorStartup();
        $share->startup_id=$id;
        $share->investor_id= $investor->id;
        $share->shares=$request->shares;
        $share->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$share]
            ,200);


    }
}
