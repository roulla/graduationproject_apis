<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Libraries\RedmineService;
use App\Http\Controllers\Controller;

class RedmineProjectController extends Controller
{
    protected $service;
    protected $connected;
    public function __construct() {
        $this->service= new RedmineService();
        $this->connected=$this->service->connectToLibrary();
    }
    public function all($name)
    {
        $private_pro=($this->connected->project->all(['name'=> $name]))['projects'];
        $public_pro=$this->connected->project->all(['is_public'=>1])['projects'];
        $merged = array_merge($private_pro,$public_pro);
        return Response()->json(['status' => 'success', 'Message' => 'Done', 'data' => $merged], 200);
    }
    public function all2()
    {
        return $this->connected->project->all();
    }

    public function show($projectId)
    {
        $project= $this->connected->project->show($projectId);
        if(empty($project)){
            return Response()->json(['status'=>'error','Message'=>'this project doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status' => 'success', 'Message' => 'Done ', 'data' =>$project], 200);

    }
    public function add(Request $request)
    {
        if(empty($request->description)){
            $request->description=" ";
        }
        $this->connected->project->create([
            'name' => $request->name,
            'identifier' => strtolower(str_replace(' ', '-', $request->name)),
            'description'=> $request->description,
            'is_public'=>1,
            'tracker_ids' => []
        ]);
        return Response()->json(['status' => 'success', 'Message' => 'Done add', 'data' => $this->connected->project->show(strtolower($request->name))], 200);

    }

    public function create_forStartUp($name,$identifier,$is_public)
    {

        $this->connected->project->create([
            'name' => $name,
            'identifier' => strtolower(str_replace(' ', '-',$identifier)),
            'is_public'=>$is_public,
            'tracker_ids' => []
        ]);
    }

    public function update(Request $request,$projectId)
    {
        $project= $this->connected->project->show($projectId);
        if(empty($project)){
            return Response()->json(['status'=>'error','Message'=>'this project doesnt exist!','data'=>''],400);
        }
        if(empty($request->description)){
            $request->description=" ";
        }
        $this->connected->project->update($projectId, [
            'description'=>$request->description
        ]);
        return Response()->json(['status' => 'success', 'Message' => 'Done Update', 'data' =>$this->connected->project->show($projectId)], 200);

    }

    public function getIdByName($name)
    {
        return $this->connected->project->getIdByName($name);
    }

    public function delete($projectId)
    {
        $project= $this->connected->project->show($projectId);
        if(empty($project)){
            return Response()->json(['status'=>'error','Message'=>'this project doesnt exist!','data'=>''],400);
        }
        if($project['project']['is_public']!=1){
        return Response()->json(['status'=>'error','Message'=>'this project related to StartUp !','data'=>''],400);
        }

        return Response()->json(['status' => 'success', 'Message' => 'Deleted it', 'data' =>$this->connected->project->remove($projectId)], 200);

    }
 }
