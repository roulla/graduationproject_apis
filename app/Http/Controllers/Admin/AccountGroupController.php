<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AccountGroup;
use App\Models\Startup;
use Validator;
use App\Http\Controllers\Controller;

class AccountGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups=AccountGroup::with(['accounts'=> function($query)  {
        $query->join('startups','startups.id','accounts.startup_id')
            ->select('accounts.*','startups.name as startup_name');
        }])->get();
        return Response()->json(['status'=>'success','Message'=>'show all Account Groups','data'=>$groups],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'message' =>  $validator->errors() ] , 400);
        }
        $group=new AccountGroup();
        $group->name=$request->name;
        $group->description=$request->description;
        $group->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$group],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group=AccountGroup::find($id);
        if (!isset($group)) {
        return Response()->json(['status'=>'error','Message'=>'this group doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$group],200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group=AccountGroup::find($id);
        if (!isset($group)) {
        return Response()->json(['status'=>'error','Message'=>'this group doesnt exist!','data'=>''],400);
        }
        if (isset($request->name)) {
           $group->name=$request->name;
        }
        if (isset($request->description)) {
           $group->description=$request->description;
        }

        $group->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$group],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $group=AccountGroup::find($id);
      if (!isset($group)) {
        return Response()->json(['status'=>'error','Message'=>'this group doesnt exist!','data'=>''],400);
        }
      $group->delete();
      return Response()->json(['status'=>'success','Message'=>'Deleted it!']
            ,200);
    }
    ///////search
    public function search($name)
    {
        $boo=false;
        $myStartup = Startup::whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->pluck('id');
        if ($myStartup->isEmpty()) {
            $myStartup='';
        }
        $groups=AccountGroup::orwhereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])
            ->orwhereHas('accounts', function ($query) use($name,$myStartup) {
                $query->whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`balance`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`status`) like ?', ['%'.strtolower($name).'%'])->
                orwhere('startup_id',$myStartup);
            })
            ->with(['accounts'=> function ($query) use($name,$myStartup) {
                $query->whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`balance`) like ?', ['%'.strtolower($name).'%'])->
                orwhereRaw('LOWER(`status`) like ?', ['%'.strtolower($name).'%'])->
                orwhere('startup_id',$myStartup);
            }])->get();
        foreach($groups as $v){
            if($v->accounts->isEmpty()){
                $boo=true;
            }}
        if($boo){
            $groups=AccountGroup::orwhereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])
                ->orwhereRaw('LOWER(`description`) like ?', ['%'.strtolower($name).'%'])
                ->orwhereHas('accounts', function ($query) use($name,$myStartup) {
                    $query->whereRaw('LOWER(`name`) like ?', ['%'.strtolower($name).'%'])->
                    orwhereRaw('LOWER(`balance`) like ?', ['%'.strtolower($name).'%'])->
                    orwhereRaw('LOWER(`status`) like ?', ['%'.strtolower($name).'%'])->
                    orwhere('startup_id',$myStartup);
                })
                ->with('accounts')->get();
        }
        if (!isset($groups)) {
            return Response()->json(['status'=>'error','Message'=>'no result!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$groups]
            ,200);
    }
}
