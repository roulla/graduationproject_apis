<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Libraries\RedmineService;
use App\Http\Controllers\Controller;
use Validator;

class IssueController extends Controller
{
    protected $service;
    protected $connected;

    public function __construct() {
        $this->service= new RedmineService();
        $this->connected=$this->service->connectToLibrary();
    }

    public function all($id)
    {
        return $this->connected->issue->all([
        'project_id' => $id,
        'sort' => 'id'
        ]);
    }

    public function all_closed($project_id)
    {
        return $this->connected->issue->all([
            'status_id' => '5',
            'project_id' =>strtolower($project_id)]);
    }

    public function closedIssue($id)
    {
        $closed_issue =$this->connected->issue->update($id, [
            'status' => 'Closed',
        ]);
        return response()->json($closed_issue);
    }

    public function store(Request $request)
    {
//        $validate_array= [
//            'project_id' => 'required|integer',
//            'subject'    => 'required|string',
//            'description' => 'required|string',
//            'priority_id'    => 'required|integer',
//            'due_date' => 'required|date',
//            'start_date' => 'required|date'
//        ];
//        $validator = Validator::make($request->all(),$validate_array);
//        if ($validator->fails()) {
//            return response()->json(['status' => 'error' , 'message' =>  $validator->errors() ] , 400);
//        }
        $statusId ='';
        foreach ($this->connected->issue_priority->all()['issue_priorities'] as $e) {
            if(strtolower($e['name'])===strtolower($request->priority_id))
                $statusId=$e['id'];
        }

           if($request->due_date<$request->start_date){
            return Response()->json(['status'=>'error','Message'=>'error','data'=>''],400);
           }
        $issue= $this->connected->issue->create([
            'subject' => $request->subject,
            'description' => "It was added by admin",
            'project_id' => $request->project_id,
            'category_id' => $request->category_id,
            'priority_id' => $statusId,
            'status_id' => $this->connected->issue_status->getIdByName($request->status_id),
            'tracker_id' => $request->tracker_id,
            'assigned_to_id' => $request->assigned_to_id,
            'author_id' => $request->author_id,
            'due_date' => $request->due_date,
            'done_ratio'=>$request->done_ratio,
            'start_date' => $request->start_date,
            'watcher_user_ids' => $request->watcher_user_ids,
            'fixed_version_id' => $request->fixed_version_id,
        ]);
        return Response()->json(['status' => 'success', 'Message' => 'Done add', 'data' =>$issue], 200);
//         $client->issue->update($issueId, [
//      'status' => 'Resolved',
// ]);
    }

    public function show($id)
    {
        return $this->connected->issue->show($id,['include'=>'attachments']);
    }

    public function allIssuePriority()
    {
        return $this->connected->issue_priority->all();
    }

    public function allIssueStatus()
    {
       return $this->connected->issue_status->all();
    }

    public function getIdStatusByName($name)
    {
        return $this->connected->issue_status->getIdByName($name);
    }

    public function  update(Request $request,$id){
//        $issue=self::show($id);
//        $update = [
//            'subject' => ($request->has('subject') && !empty($request->subject)) ? $request->subject : $issue->subject,
//            'status' => ($request->has('status') && !empty($request->status)) ? $request->status : $issue->status ,
//            'priority_id' => ($request->has('priority_id') && !empty($request->priority_id)) ? $request->priority_id : $issue->priority_id ,
//            'due_date' => ($request->has('due_date') && !empty($request->due_date)) ? $request->due_date : $issue->due_date ,
//            'description' => ($request->has('description') && !empty($request->description)) ? $request->description : $issue->description ,
//            'start_date' => ($request->has('start_date') && !empty($request->start_date)) ? $request->start_date : $issue->start_date ,
//            'done_ratio' => ($request->has('done_ratio') && !empty($request->done_ratio)) ? $request->done_ratio : $issue->done_ratio ,
//            'estimated_hours' => ($request->has('estimated_hours') && !empty($request->estimated_hours)) ? $request->estimated_hours : $issue->estimated_hours
//
//        ];
        $priority ='';
        foreach ($this->connected->issue_priority->all()['issue_priorities'] as $e) {
            if(strtolower($e['name'])===strtolower($request->priority_id))
                $priority=$e['id'];
        }
       
        $MyIssue=$this->connected->issue->show($id);
        if(!empty($MyIssue)){
        $updated_issue= $this->connected->issue->update($id,[
            'subject' => $request->subject,
            'description' => "It was edited by admin",
            'category_id' => $request->category_id,
            'priority_id' =>$priority,
            'status_id' =>$this->connected->issue_status->getIdByName($request->status_id),
            'tracker_id' => $request->tracker_id,
            'assigned_to_id' => $request->assigned_to_id,
            'author_id' => $request->author_id,
            'due_date' => $request->due_date,
            'done_ratio'=>$request->done_ratio,
            'start_date' => $request->start_date,
            'watcher_user_ids' => $request->watcher_user_ids,
            'fixed_version_id' => $request->fixed_version_id,
        ]);
   return Response()->json(['status' => 'success', 'Message' => 'update it', 'data' =>$updated_issue], 200);
    }
        return Response()->json(['status'=>'error','Message'=>'this issue doesnt exist!','data'=>''],400);
    }

    public function destroy($id){
        $this->connected->issue->remove($id);
        return response()->json(['status'=>'success','Message'=>'Deleted it!'],200);
    }

    public function attach(Request $request,$id)
    {
        $MyIssue=$this->connected->issue->show($id,['include'=>'attachments']);
        if(empty($MyIssue['issue']['attachments'])) {
        $upload = json_decode($this->connected->attachment->upload($request->filecontent));
        $this->connected->issue->attach($id, [
            'token' => $upload->upload->token,
            'filename' =>$request->filename.'.'.$request->filecontent->extension(),
            'description' => $request->description,
            'content_type' =>'application/'.$request->filecontent->getMimeType(),
        ]);
        return Response()->json(['status' => 'success', 'Message' => 'upload it!'], 200);}
        else{
            return Response()->json(['status'=>'error','Message'=>'this issue have file!','data'=>''],400);
        }

       }

    public function download($id)
    {
        $file_content =$this->connected->issue->show($id,['include'=>'attachments']);
        // $contents =file_put_contents($request->filename, $file_content);
        $url=$file_content['issue']['attachments'][0]['content_url'];
        return Response()->json(['status' => 'success', 'Message' => 'download url', 'data' => $url], 200);

    }
public function pickDate(Request $request,$startupName)
    {
 $issue=[$this->connected->issue->create([
            'subject' => $request->subject,
            'description' =>$request->description,
            'project_id' => strtolower($startupName),
            'due_date' => $request->due_date,
            'start_date' => $request->start_date,
        ])];

 $issue= $this->connected->issue->update($issue[0]->id,[
            'status_id' =>$this->connected->issue_status->getIdByName('Rejected'),
        ]);
        return Response()->json(['status' => 'success', 'Message' => 'done', 'data' => $issue], 200);
    }
    public function get_pickDate($startupName)
    {
  return $this->connected->issue->all([
            'status_id' => '6',
            'project_id' =>strtolower($startupName)]);
    }

}
