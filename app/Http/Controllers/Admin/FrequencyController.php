<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Frequency;
use Validator;
use App\Http\Controllers\Controller;

class FrequencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frequency=Frequency::with('kpis')->get();
        return Response()->json(['status'=>'success','Message'=>'show all frequency','data'=>$frequency],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        $frequency=new Frequency();
        $frequency->name=$request->name;
        $frequency->description=$request->description;
        $frequency->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$frequency]
            ,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $frequency=Frequency::find($id);
        if (!isset($frequency)) {
        return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$frequency]
            ,200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $frequency=Frequency::find($id);
        if (!isset($frequency)) {
        return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }

        if (isset($request->name)) {
           $frequency->name=$request->name;
        }
        if (isset($request->description)) {
           $frequency->description=$request->description;

        }
        
        $frequency->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$frequency]
            ,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $frequency=Frequency::find($id);
      if (!isset($frequency)) {
        return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }
      $frequency->delete();
      return Response()->json(['status'=>'success','Message'=>'Deleted it!'] ,200);
    }
}
