<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;


class UserController extends Controller
{
    private function UpdateValidationRules($id= null)
    {
        return [
            'name' => 'required|string|min:3|max:100',
            'email'      => 'required|email|unique:users,email,'.$id,
            'password'   => 'nullable|min:5|max:24|confirmed',
        ];
    }

    public function index()
    {
        $users = User::all();
        $users=DB::table('users')->get();
        return Response()->json(['status'=>'success','Message'=>'show all Users','data'=>$users],200);
    }

    public function show($id)
    {
        $user=User::find($id);
        if (!isset($user)) {
            return Response()->json(['status'=>'error','Message'=>'this user doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$user],200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:100',
            'email'      => 'required|email|unique:users,email',
            'password'   => 'required|min:5|max:24|confirmed',
            'password_confirmation'=>''
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        //$user->password_confirmation=bcrypt($request->password_confirmation);
        $user->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$user],200);
    }

    public function update(Request $request, $id)
    {
        $user=User::find($id);
        if (!isset($user)) {
            return Response()->json(['status'=>'error','Message'=>'this user doesnt exist!','data'=>''],400);
        }

        if (isset($request->name)) {
            $user->name=$request->name;
        }
        if (isset($request->email)) {
            $user->email=$request->email;
        }
        if (isset($request->password)) {
            $user->password=bcrypt($request->password);
        }
        $user->save();

        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$user],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $user = User::findOrFail($id);
            $deleted =  $user->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'Message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'Message' => 'fail_while_delete']);
            }
    }
}