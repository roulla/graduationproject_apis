<?php

namespace App\Http\Controllers\Startup;

use App\Models\StartupMember;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Account;
use App\Models\TransactionType;
use Validator;
use App\Http\Controllers\Controller;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $myAccounts=Account::where('startup_id',$myStartup)->get('id')->toArray();

        $transactions=Transaction::whereIn('credit_id' , $myAccounts)
            ->orWhereIn('debit_id' , $myAccounts)->get();

            if ($transactions->isEmpty()) {
                return Response()->json(['status' => 'error', 'Message' => 'no Transactions!', 'data' => ''], 400);
            }

        return Response()->json(['status'=>'success','Message'=>'show all Transactions','data'=>$transactions],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'credit_id' => 'required',
            'debit_id' => 'required',
            'type_id' => 'required',
            'description' => 'required',
            'amount'    => 'required|numeric|min:1',
            'date' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }


        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $myAccounts=Account::where('startup_id',$myStartup)->get();
        foreach ($myAccounts as $myAccount)
        {
            $newBalance=0;
            if($myAccount->id ==$request->debit_id){

                $debitAccount = Account::find($request->debit_id);

                //validation type
                $type = TransactionType::find($request->type_id);
                if (!isset($type)) {
                    return Response()->json(['status'=>'error','Message'=>'this type doesnt exist!','data'=>''],400);
                }
                //validate Credit Account
                $creditAccount= Account::find($request->credit_id);
                if (!isset($creditAccount)) {
                    return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
                }

                //check have money
                $amount = $request->amount;

               // if($debitAccount->parent_id == null) {
                if ($amount > $creditAccount->balance) {
                        return Response()->json(['status' => 'failed', 'Message' => 'No money enough']
                            , 400);
                }
                    $newBalance = $debitAccount->balance + $amount;
               // }

               
                    //success Add
                    $transaction=new Transaction();
                    $transaction->credit_id=$request->credit_id;
                    $transaction->debit_id=$request->debit_id;
                    $transaction->type_id=$request->type_id;
                    $transaction->description=$request->description;
                    $transaction->amount=$request->amount;
                    $transaction->date=$request->date;


                    $newBalance2= $creditAccount->balance - $amount;
                    $debitAccount->balance = $newBalance;
                    $creditAccount->balance = $newBalance2;
                    $creditAccount->save();
                    $debitAccount->save();
                    $transaction->save();

                    return Response()->json(['status'=>'success','Message'=>'Done create Transaction','data'=>$transaction]
                        ,200);
                
            }
        }
        return Response()->json(['status'=>'failed','Message'=>'cant Do This transaction its should Debit Account for Yours' ]
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $myAccounts=Account::where('startup_id',$myStartup)->get('id')->toArray();

        $transactions=Transaction::whereIn('credit_id' , $myAccounts)
                                  ->orWhereIn('debit_id' , $myAccounts)->get();
        $transactionToSearch=Transaction::find($id);
        foreach ($transactions as $transaction)
        {
            if($transaction==$transactionToSearch)
            {
                return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$transactionToSearch]
                    ,200);
            }
        }
        return Response()->json(['status'=>'error','Message'=>'this transaction doesnt exist!','data'=>''],400);
    }
    public function GetAccounttrans($id)
    {
        $transaction = Transaction::where('credit_id',$id)->orwhere('debit_id',$id)->orderby('date')->get()
            ->groupBy(function($item) {
                return Carbon::createFromFormat('Y-m-d', $item->date )->format('Y');
            } )->map(function ($items) {
                return $items->groupBy(function($item2) {
                    return Carbon::createFromFormat('Y-m-d', $item2->date )->format('M');
                });});

        if ($transaction->isEmpty()) {
            return Response()->json(['status'=>'error','Message'=>'no result!','data'=>''],400);
        }
        $data=[];
        $temp=[];
        $temp2=[];
        foreach($transaction as $key=> $item){
            foreach ($item as $key2=>$value){
                $index=0;

                foreach ($value as $element){
                    $credit_name = Account::where('id',$element->credit_id)->pluck('name');
                    $debit_name = Account::where('id',$element->debit_id)->pluck('name');
                    $type_name=TransactionType::where('id',$element->type_id)->pluck('name');
                    $value[$index]["credit_name"]= $credit_name[0];
                    $value[$index]["debit_name"]= $debit_name[0];
                    $value[$index]["type_name"]= $type_name[0];
                    $index++;
                }

                $temp2['month_date']=$key2;
                $temp2['month_transactions']=$value;
                array_push($temp,$temp2);

            }
            $temp3['year']=$key;
            $temp3['months']=$temp;
            array_push( $data,$temp3);
            $temp=[];
        }


        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$data]
            ,200);
    }
}
