<?php

namespace App\Http\Controllers\Startup;

use App\Models\StartupMember;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator;
use App\Http\Controllers\Controller;
use Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
           $categories=Category::with(['kpis'=> function($query) use($myStartup) {
            $query->where('startup_id',$myStartup)->join('frequencies','frequencies.id','k_p_i_s.frequency_id')->select('k_p_i_s.*','frequencies.name as frequency_name');
        }])->get();
    
        return Response()->json(['status'=>'success','Message'=>'show all Categories','data'=>$categories],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        $category=new Category();
        $category->name=$request->name;
        $category->description=$request->description;
        $category->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$category]
            ,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=Category::find($id);
        if (!isset($category)) {
        return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$category]
            ,200);
    }
}
