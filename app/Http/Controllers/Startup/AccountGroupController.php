<?php

namespace App\Http\Controllers\Startup;

use App\Models\StartupMember;
use Illuminate\Http\Request;
use App\Models\AccountGroup;
use Validator;
use App\Http\Controllers\Controller;
use Auth;

class AccountGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $groups=AccountGroup::with(['accounts' => function($query) use ($myStartup) {
            $query->where('startup_id',$myStartup)
            ->join('startups','startups.id','accounts.startup_id')
                ->select('accounts.*','startups.name as startup_name');
        }])->get();
        return Response()->json(['status'=>'success','Message'=>'show all Account Groups','data'=>$groups],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }
        $group=new AccountGroup();
        $group->name=$request->name;
        $group->description=$request->description;
        $group->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$group],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group=AccountGroup::find($id);
        if (!isset($group)) {
        return Response()->json(['status'=>'error','Message'=>'this group doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$group],200);
    }
}
