<?php

namespace App\Http\Controllers\Startup;

use Illuminate\Http\Request;
use App\Models\StartupMember;
use App\Models\Startup;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class StartupMemberController extends Controller
{

    public function index()
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $members=StartupMember::where('startup_id',$myStartup)->get();

        return Response()->json(['status'=>'success','Message'=>'show all members in same my team ','data'=>$members],200);
    }


    public function store(Request $request)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric',
            'shares' => 'required|numeric|max:100|min:0',
            'avatar' => 'nullable'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }


        $member=new StartupMember();
        $member->name=$request->name;
        $member->position=$request->position;
        $member->address=$request->address;
        $member->phone=$request->phone;
        $member->shares=$request->shares;
        $member->startup_id=$myStartup;
        $member->user_id = $request->user_id;
        if (isset($request->avatar)) {
            $member->avatar = Storage::disk('public_images')->put('members', $request->file('avatar'));
        }
         if(isset($request->user_id)){
        $userId=DB::table('role_user')->where('user_id', '=', $request->user_id)->get();
        if(!$userId->isEmpty()){
              return Response()->json(['status'=>'error','Message'=>'this userId is taken !','data'=>''],400);
        }
        DB::table('role_user')->insert(
            ['user_id' => $request->user_id, 'role_id' => '2']
        );}
        $member->save();
      
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$member]
            ,200);
    }



public function getUsres()
{
    $users=DB::table('users')->get();
    if (!isset($users)) {
        return Response()->json(['status'=>'error','Message'=>'there is no users!','data'=>''],400);
    }
    return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$users]
        ,200);
}


    public function show($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $myMembers=StartupMember::where('startup_id',$myStartup)->get();

        $member=StartupMember::find($id);
        foreach ($myMembers as $key => $value) {
            if ($value==$member) {
                return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$member],200);
            }
        }

        return Response()->json(['status'=>'error','Message'=>'this Member doesnt exist!','data'=>''],400);
    }


    public function update(Request $request, $id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $myMember=StartupMember::where('startup_id',$myStartup)->get();

        $member=StartupMember::find($id);

        foreach ($myMember as $key => $value) {
            if ($value==$member) {
                $validator = Validator::make($request->all(), [
                    'phone' => 'numeric',
                    'shares' => 'numeric|max:100|min:0'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
                }

                if (isset($request->name)) {
                    $member->name=$request->name;
                }
                if (isset($request->position)) {
                    $member->position=$request->position;
                }
                if (isset($request->address)) {
                    $member->address=$request->address;
                }
                if (isset($request->phone)) {
                    $member->phone=$request->phone;
                }
                if (isset($request->shares)) {
                    $member->shares=$request->shares;
                }
                if (isset($request->avatar)) {
                    $member->avatar = Storage::disk('public_images')->put('members', $request->file('avatar'));
                }
                $member->startup_id=$myStartup;
                $member->save();
                return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$member],200);

            }
        }
        return Response()->json(['status'=>'error','Message'=>'this member doesnt exist!','data'=>''],400);
    }


    public function destroy($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $myMember=StartupMember::where('startup_id',$myStartup)->get();

        $member=StartupMember::find($id);

        foreach ($myMember as $key => $value) {
            if ($value==$member) {
                DB::table('role_user')->where('user_id',$member['user_id'])->delete();
                $member->delete();
                return Response()->json(['status'=>'success','Message'=>'Deleted it!'],200);
            }
        }
        return Response()->json(['status'=>'error','Message'=>'this member doesnt exist!','data'=>''],400);
    }
}
