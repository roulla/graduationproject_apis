<?php

namespace App\Http\Controllers\Startup;

use Illuminate\Http\Request;
use App\Models\Frequency;
use Validator;
use App\Http\Controllers\Controller;

class FrequencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frequency=Frequency::all();
        return Response()->json(['status'=>'success','Message'=>'show all frequency','data'=>$frequency],200);
    }

    public function show($id)
    {
        $frequency=Frequency::find($id);
        if (!isset($frequency)) {
        return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$frequency]
            ,200);
    }

}
