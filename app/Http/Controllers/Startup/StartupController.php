<?php

namespace App\Http\Controllers\Startup;

use Illuminate\Http\Request;
use App\Models\Startup;
use Validator;
use App\Http\Controllers\Controller;
use DB;

class StartupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //turn "test";
       $startups=Startup::all();
       return Response()->json(['status'=>'success','Message'=>'show all startups','data'=>$startups],200); 
    }

    public function show($id)
    {
        $startup=Startup::findOrFail($id);
//        if (!isset($startup)) {
//        return Response()->json(['status'=>'error','Message'=>'this startup doesnt exist!','data'=>''],400);
//        }
        return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$startup]
            ,200);
    }
    
}
