<?php

namespace App\Http\Controllers\Startup;

use App\Models\Frequency;
use App\Models\StartupMember;
use Illuminate\Http\Request;
use App\Models\KPI_Data;
use Carbon\Carbon;
use App\Models\KPI;
use Validator;
use App\Http\Controllers\Controller;
use Auth;

class KPIDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $kpi=KPI::where('startup_id',$myStartup)->get('id')->toArray();

        $data=KPI_Data::whereIn('kpi_id',$kpi)->orderBy('date', 'desc')->get();
        return Response()->json(['status'=>'success','Message'=>'show all data of kpi','data'=>$data],200);
    }
public function getTotal($kpiId)
    {
        $kpi=KPI::findOrFail($kpiId);
        $data=$kpi->datas;
        $total=0;
        //$data=KPI_Data::with('datas')->get('actual');
         foreach ($data as $v) {
         $total+=$v->actual;  
        }
        return Response()->json(['status'=>'success','Message'=>'total value','data'=>$total],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $kpis=KPI::where('startup_id',$myStartup)->get();

        foreach ($kpis as $kpi) {
           // dd('roro');
            if ($kpi->id == $request->kpi_id) {
                //dd('roro');
                $validator = Validator::make($request->all(), [
                    'actual' => 'required|numeric|min:0',
                ]);
                if ($validator->fails()) {
                    return response()->json(['status' => 'error', 'Message' => $validator->errors()], 400);
                }

                $frequency=Frequency::where('id','=',$kpi->frequency_id)->pluck('name');
                //add success
                // $Date_now = Carbon::today()->format('Y-m-d');
                $Date_now=date('Y-m-d', strtotime($request->date));
                $last_inserted_data=KPI_Data::where('kpi_id','=',$request->kpi_id)->orderBy('id', 'DESC')->get()->first();
                if (isset($last_inserted_data)) {
                    $last_date=Carbon::parse($last_inserted_data->date)->format('Y-m-d');

                    if($frequency[0] == 'daily')
                    {
                        $next_day= date('Y-m-d', strtotime($last_date. ' + 1 days'));
                        if($Date_now<$next_day){
                            return Response()->json(['status'=>'error','Message'=>'the day is not over you have to wait to '.$next_day,'data'=>''],400);
                        }
                    }

                    elseif($frequency[0] == 'weekly')
                    {
                        $next_week= date('Y-m-d', strtotime($last_date. ' + 7 days'));
                        if($Date_now<$next_week){
                            return Response()->json(['status'=>'error','Message'=>'the week is not over you have to wait to '.$next_week,'data'=>''],400);
                        }
                    }

                    elseif($frequency[0] == 'monthly')
                    {
                        $next_month= date('Y-m-d', strtotime($last_date. ' + 30 days'));
                        if($Date_now<$next_month){
                            return Response()->json(['status'=>'error','Message'=>'the month is not over you have to wait to '.$next_month,'data'=>''],400);
                        }

                    }
                }
                //add success
                $Date = Carbon::today()->format('Y-m-d');

                $data = new KPI_Data();
                $data->kpi_id = $request->kpi_id;
                $data->actual = $request->actual;
                /*if(!empty($last_inserted_data)){
                $data->actual=$last_inserted_data->actual+$request->actual;}
                else{
                    $data->actual=$request->actual;
                }*/
                $data->date = $Date_now;
                $data->save();

                return Response()->json(['status' => 'success', 'Message' => 'Done Add', 'data' => $data], 200);
            }
        }
                return Response()->json(['status'=>'failed','Message'=>'Not Found KPI'],400);
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $kpi=KPI::where('startup_id',$myStartup)->get('id')->toArray();
        $data=KPI_Data::whereIn('kpi_id',$kpi)->get();

        $dataSearch = KPI_Data::find($id);
        foreach ($data as $key => $value) {
            if ($value == $dataSearch) {
                return Response()->json(['status' => 'success', 'Message' => 'found it!', 'data' => $dataSearch], 200);
            }
        }
        return Response()->json(['status' => 'error', 'Message' => 'this data doesnt exist!'], 400);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $kpi=KPI::where('startup_id',$myStartup)->get('id')->toArray();
        $data=KPI_Data::whereIn('kpi_id',$kpi)->get();

        $dataToUpdate = KPI_Data::find($id);
        foreach ($data as $key => $value) {
            if ($value == $dataToUpdate) {
                $validator = Validator::make($request->all(), [
                    'actual' => 'numeric|min:0'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
                }
                if (isset($request->actual)) {
                   /* $old_actual=$dataToUpdate->actual;
                    $new_actual=$request->actual-$old_actual;
                    $dataToUpdate->actual=$request->actual;
                    $last_inserted_datas=KPI_Data::where('kpi_id',$dataToUpdate->kpi_id)->where('date','>',$dataToUpdate->date)->get();
                    foreach ($last_inserted_datas as $v){
                        $v->actual=$v->actual+$new_actual;
                        $v->save();
                    }*/
                     $dataToUpdate->actual=$request->actual;
                }
                if (isset($request->date)) {
                    return Response()->json(['status'=>'error','Message'=>'you cannot change the date!','data'=>''],400);
                }
                $dataToUpdate->save();
                return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$dataToUpdate],200);
            }
        }
        return Response()->json(['status' => 'error', 'Message' => 'this data doesnt exist!'], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $kpi=KPI::where('startup_id',$myStartup)->get('id')->toArray();
        $data=KPI_Data::whereIn('kpi_id',$kpi)->get();

        $dataToDelete = KPI_Data::find($id);
        foreach ($data as $key => $value) {
            if ($value == $dataToDelete) {
                $dataToDelete->delete();
                return Response()->json(['status'=>'success','Message'=>'Deleted it!'],200);
            }
        }
        return Response()->json(['status' => 'error', 'Message' => 'this data doesnt exist!'], 400);
    }
}
