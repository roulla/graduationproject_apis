<?php

namespace App\Http\Controllers\Startup;

use App\Models\StartupMember;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\AccountGroup;
use Validator;
use App\Http\Controllers\Controller;
use Auth;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Global variable
    const OPENED = 'opened';
    const CLOSED = 'closed';


    public function index(Request $request)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');

        $status = $request->status;

        $accounts = Account::where(
            [
                'status' => $status,
                'startup_id'  => $myStartup
            ])->get();
        return Response()->json(['status'=>'success','Message'=>'show all Accounts','data'=>$accounts],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $parent_id = Account::where(['startup_id' => $myStartup ,
                                     'status' => 'opened'])->pluck('id');
        //validation request
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'balance'    => 'required|numeric|min:0',
            'group_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        $group=AccountGroup::find($request->group_id);
        if (!isset($group)) {
        return Response()->json(['status'=>'error','Message'=>'this group doesnt exist!','data'=>''],400);
        }

       /*if (isset($request->parent_id)){
        $openAccount = Account::where(
            [
                'status' => 'opened',
                'startup_id'  => $myStartup
            ])->find($request->parent_id);
        if (!isset($openAccount)) {
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
        }
    }*/

        //add new account
        $account=new Account();
        $account->name=$request->name;
        $account->balance=$request->balance;
        $account->status=self::OPENED;
        $account->startup_id=$myStartup;
        $account->group_id=$request->group_id;
        $account->parent_id=$parent_id[0];
        $account->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$account]
            ,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $myAccounts=Account::where('startup_id',$myStartup)->get();

        $account=Account::find($id);
        foreach ($myAccounts as $key => $value) {
            if ($value==$account) {
                return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$account],200);

            }
        }
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $myAccounts=Account::where('startup_id',$myStartup)->get();

        $account=Account::find($id);
        foreach ($myAccounts as $key => $value) {
            if ($value == $account) {
                $validator = Validator::make($request->all(), [
                    'balance' => 'numeric|min:0'
                ]);
                if ($validator->fails()) {
                    return response()->json(['status' => 'error', 'Message' => $validator->errors()], 400);
                }

                if (isset($request->name)) {
                    $account->name = $request->name;
                }
                if (isset($request->balance)) {
                    $account->balance = $request->balance;
                }
                if (isset($request->status)) {
                    $account->status = $request->status;
                }

                if (isset($request->group_id)) {
                    $group = AccountGroup::find($request->group_id);
                    if (!isset($group)) {
                        return Response()->json(['status' => 'error', 'Message' => 'this group doesnt exist!', 'data' => ''], 400);
                    }
                    $account->group_id = $request->group_id;
                }
                if (isset($request->parent_id)) {
                    $openAccount = Account::where(
                        [
                            'status' => 'opened',
                            'startup_id'  => $myStartup
                        ])->find($request->parent_id);

                    if (!isset($openAccount)) {
                        return Response()->json(['status' => 'error', 'Message' => 'this account doesnt exist!', 'data' => ''], 400);
                    }
                    $account->parent_id = $request->parent_id;
                }

                $account->save();
                return Response()->json(['status' => 'success', 'Message' => 'Done Update', 'data' => $account], 200);
            }
        }
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!'],400);

    }

    public function close($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $myAccounts=Account::where('startup_id',$myStartup)->get();

        $account=Account::find($id);
        foreach ($myAccounts as $key => $value) {
            if ($value==$account) {
                $account->status=self::CLOSED;
                $account->save();
                return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$account],200);

            }
        }
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!','data'=>''],400);
    }

    public function ReviewAllCreditTransactions($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $myAccounts=Account::where('startup_id',$myStartup)->get();

        $account=Account::find($id);
        foreach ($myAccounts as $key => $value) {
            if ($value==$account) {
                $transactions=$account->creditTransactions;
                return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$transactions],200);

            }
        }
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!'],400);
    }

    public function ReviewAllDebitTransactions($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $myAccounts=Account::where('startup_id',$myStartup)->get();

        $account=Account::find($id);
        foreach ($myAccounts as $key => $value) {
            if ($value==$account) {
                $transactions=$account->debitTransactions;
                return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$transactions],200);

            }
        }
        return Response()->json(['status'=>'error','Message'=>'this account doesnt exist!'],400);
    }
}
