<?php

namespace App\Http\Controllers\Startup;
use App\Models\StartupMember;
use App\Models\Startup;
use Illuminate\Http\Request;
use App\Libraries\RedmineService;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use function MongoDB\BSON\toJSON;

class IssueController extends Controller
{
    protected $service;
    protected $connected;

    public function __construct()
    {
        $this->service = new RedmineService();
        $this->connected = $this->service->connectToLibrary();
    }

    public function allproject()
    {
          $currentUser = Auth::user()->id;
          $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
          $Startup = Startup::where('id','=',$myStartup)->pluck('name');
        $private_pro=($this->connected->project->all(['name'=> $Startup[0]]))['projects'];
        $public_pro=$this->connected->project->all(['is_public'=>1])['projects'];
        $merged = array_merge($private_pro,$public_pro);
        return Response()->json(['status' => 'success', 'Message' => 'all projects', 'data' => $merged], 200);
    }

    public function allissue($project)
    {
      /*  $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id','=',$myStartup)->pluck('name');
        $pro= $this->connected->project->show(strtolower($project));
        if(strtolower($project)==strtolower($Startup[0])||$pro['project']['is_public']==1) {
            return $this->connected->issue->all(['project_id' => strtolower($project)]);
        }*/
        return $this->connected->issue->all(['project_id' => strtolower($project)]);
    }

    public function showissue($id)
    {
        $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id',$myStartup)->pluck('name');
        $MyIssue=$this->connected->issue->show($id);
        $IssueProject=$MyIssue['issue']['project']['name'];
        if(empty($IssueProject)){
            return Response()->json(['status' => 'error', 'Message' => 'doesnt exist!', 'data' => ''], 400);
        }
       $pro= $this->connected->project->show(strtolower($IssueProject));
       $is_public=$pro['project']['is_public'];
        if(strtolower($IssueProject)===strtolower($Startup[0])||$is_public==1) {
            return Response()->json(['status'=>'success','Message'=>'found it!', 'data' => $this->connected->issue->show($id,['include'=>'attachments'])],200);
        }
        return Response()->json(['status' => 'error', 'Message' => 'doesnt exist!', 'data' => ''], 400);
         }

    public function closedIssue($project)
    {
         return $this->connected->issue->all([
             'status_id' => '5',
             'project_id' =>strtolower($project)]);
    }


    public function createissue(Request $request,$project)
    {
        $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id',$myStartup)->pluck('name');
        $validator = Validator::make($request->all(), [
            'subject' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'Message' => $validator->errors()], 400);
        }
        $statusId ='';
        foreach ($this->connected->issue_priority->all()['issue_priorities'] as $e) {
            if(strtolower($e['name'])===strtolower($request->priority_id))
                $statusId=$e['id'];
        }
         if($request->due_date<$request->start_date){
            return Response()->json(['status'=>'error','Message'=>'error','data'=>''],400);
           }
       $is= $this->connected->issue->create([
            'subject' => $request->subject,
            'description' =>"It was added by ".$Startup[0],
            'project_id' => $project,
            'category_id' => $request->category_id,
            'priority_id' => $statusId,
            'status_id' => $this->connected->issue_status->getIdByName($request->status_id),
            'tracker_id' => $request->tracker_id,
            'assigned_to_id' => $request->assigned_to_id,
            'author_id' => $request->author_id,
            'due_date' => $request->due_date,
            'done_ratio'=>$request->done_ratio,
            'start_date' => $request->start_date,
            'watcher_user_ids' => $request->watcher_user_ids,
            'fixed_version_id' => $request->fixed_version_id,
        ]);
       // $this->connected->issue->setIssueStatus( $request->status_id);
        return Response()->json(['status' => 'success', 'Message' => 'add it!'], 200);
    }

    public function updateissue(Request $request, $id)
    {

        $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id',$myStartup)->pluck('name');
         $MyIssue=$this->connected->issue->show($id);
        if(empty($MyIssue)){
            return Response()->json(['status'=>'error','Message'=>'this issue doesnt exist!','data'=>''],400);
        }
        $IssueProject=$MyIssue['issue']['project']['name'];
        if(empty($IssueProject)){
            return Response()->json(['status' => 'error', 'Message' => 'doesnt exist!', 'data' => ''], 400);
        }
        $pro= $this->connected->project->show($MyIssue['issue']['project']['id']);
        $is_public=$pro['project']['is_public'];
        $statusId ='';
        foreach ($this->connected->issue_priority->all()['issue_priorities'] as $e) {
            if(strtolower($e['name'])===strtolower($request->priority_id))
                $statusId=$e['id'];
        }
        if(strtolower($IssueProject)===strtolower($Startup[0])||$is_public==1) {
             $this->connected->issue->update($id, [
                'subject' => $request->subject,
                'description' => "It was edited by ".$Startup[0],
                'category_id' => $request->category_id,
                'priority_id' => $statusId,
                'status_id' => $this->connected->issue_status->getIdByName($request->status_id),
                'tracker_id' => $request->tracker_id,
                'assigned_to_id' => $request->assigned_to_id,
                'author_id' => $request->author_id,
                'due_date' => $request->due_date,
                'done_ratio'=>$request->done_ratio,
                'start_date' => $request->start_date,
                'watcher_user_ids' => $request->watcher_user_ids,
                'fixed_version_id' => $request->fixed_version_id,

            ]);

            return Response()->json(['status' => 'success', 'Message' => 'Done Update', 'data' => $this->connected->issue->show($id,['include'=>'attachments'])], 200);
        }

        }

    public function deleteissue($id)
    {
        $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id',$myStartup)->pluck('name');
        $MyIssue=$this->connected->issue->show($id);
        $IssueProject=$MyIssue['issue']['project']['name'];
        if(empty($IssueProject)){
            return Response()->json(['status' => 'error', 'Message' => 'doesnt exist!', 'data' => ''], 400);
        }
        $pro= $this->connected->project->show($MyIssue['issue']['project']['id']);
        $is_public=$pro['project']['is_public'];
        if($IssueProject===$Startup[0]||$is_public==1) {
        $this->connected->issue->remove($id);
        return Response()->json(['status' => 'success', 'Message' => 'Deleted it!'], 200);
        }
        return Response()->json(['status'=>'error','Message'=>'this issue doesnt exist!','data'=>''],400);
    }

    public function attach(Request $request,$id)
    {
        $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id',$myStartup)->pluck('name');
        $MyIssue=$this->connected->issue->show($id,['include'=>'attachments']);
        $IssueProject=$MyIssue['issue']['project']['name'];
        if($IssueProject===$Startup[0]&& empty($MyIssue['issue']['attachments'])) {
        $upload = json_decode($this->connected->attachment->upload($request->filecontent));
        $this->connected->issue->attach($id, [
            'token' => $upload->upload->token,
            'filename' =>$request->filename.'.'.$request->filecontent->extension(),
            'description' => $request->description,
            'content_type' =>'application/'.$request->filecontent->getMimeType(),
        ]);
            return Response()->json(['status' => 'success', 'Message' => 'upload it!'], 200);
        }
        return Response()->json(['status'=>'error','Message'=>'!!','data'=>''],400);
    }
    public function allstatus()
    {
        $array=[];
          foreach ($this->connected->issue_status->all()['issue_statuses'] as $var){
              foreach ([$var] as $p){
                //  unset($p['is_closed']);
          array_push($array,$p['name']);
              }
          }
       // $array=$this->connected->issue_status->all()['issue_statuses'];
          return Response()->json(['status' => 'success', 'Message' => 'status list','data'=>$array], 200);

    }

    public function getissueStatusesIdByName($name)
    {
        $statusId=$this->connected->issue_status->getIdByName($name);
        if($statusId) {
            return Response()->json(['status' => 'success', 'Message' => 'status list', 'data' => $statusId], 200);
        }
        return Response()->json(['status'=>'error','Message'=>'this status doesnt exist!','data'=>''],400);

    }
    public function allPriorities()
    {
        $array=[];
        foreach ($this->connected->issue_priority->all()['issue_priorities'] as $var){
            foreach ([$var] as $p){
                array_push($array,$p['name']);
            }
        }
        return Response()->json(['status' => 'success', 'Message' => 'priority list','data'=>$array], 200);
    }
    public function getPrioritiesByName($name)
    {
        $statusId ='';
        foreach ($this->connected->issue_priority->all()['issue_priorities'] as $e) {
            if($e['name']===$name)
            $statusId=$e['id'];
        }
        if($statusId) {
            return Response()->json(['status' => 'success', 'Message' => 'priority list', 'data' => $statusId], 200);
        }
        return Response()->json(['status'=>'error','Message'=>'this priority doesnt exist!','data'=>''],400);

    }
    public function download(Request $request,$id)
    {

         $file_content =$this->connected->issue->show($id,['include'=>'attachments']);
        // $contents =file_put_contents($request->filename, $file_content);
        $url=$file_content['issue']['attachments'][0]['content_url'];
        return Response()->json(['status' => 'success', 'Message' => 'download url', 'data' => $url], 200);

    }

    public function pickDate(Request $request)
    {
         $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id',$myStartup[0])->pluck('name');
$issue=[$this->connected->issue->create([
            'subject' => $request->subject,
            'description' =>$request->description,
            'project_id' => strtolower($Startup[0]),
            'due_date' => $request->due_date,
            'start_date' => $request->start_date,
        ])];

$issue= $this->connected->issue->update($issue[0]->id,[
            'status_id' =>$this->connected->issue_status->getIdByName('Rejected'),
        ]);
        return Response()->json(['status' => 'success', 'Message' => 'download url', 'data' => $issue], 200);
    }
    public function get_pickDate()
    {
         $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id',$myStartup)->pluck('name');
  return $this->connected->issue->all([
            'status_id' => '6',
            'project_id' =>strtolower($Startup[0])]);
    }
}
