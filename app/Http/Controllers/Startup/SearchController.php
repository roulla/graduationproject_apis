<?php

namespace App\Http\Controllers\Startup;
use App\Libraries\RedmineService;
use App\Models\Startup;
use App\Models\StartupMember;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class SearchController extends Controller
{

    protected $service;
    protected $connected;

    public function __construct()
    {
        $this->service = new RedmineService();
        $this->connected = $this->service->connectToLibrary();
    }
    public function search($string)
    {
        $currentUser = Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $Startup = Startup::where('id',$myStartup)->pluck('name');
        $Myproject=strtolower($Startup[0]);
        $result=[];
       $Mysearch =$this->connected->search->search($string);
       foreach($Mysearch['results'] as $var){
            if (strpos($var['url'],$Myproject)!==false){
                array_push($result, $var);
            }
       }
       if(empty($result))
           return Response()->json(['status'=>'error','Message'=>'no result','data'=>''],400);
        return Response()->json(['status'=>'success','Message'=>'search result','data'=>$result],200);
}}
