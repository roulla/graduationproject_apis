<?php

namespace App\Http\Controllers\Startup;

use Illuminate\Http\Request;
use App\Models\KPI;
use Validator;
use App\Models\Startup;
use App\Models\Frequency;
use App\Models\Category;
use Auth;
use App\Models\StartupMember;
use App\Http\Controllers\Controller;

class KPIController extends Controller
{
    public function getMyKPIs()
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->pluck('startup_id');
        $kpi=KPI::where('startup_id',$myStartup)
            ->join('frequencies','frequencies.id','k_p_i_s.frequency_id')
            ->join('categories','categories.id','k_p_i_s.category_id')
            ->select('k_p_i_s.*','frequencies.name as frequency_name','categories.name as category_name')
            ->get();
        return Response()->json(['status'=>'success','data'=>$kpi],200);
    }

    public function addMyKPI(Request $request)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'frequency_id' => 'required',
            'target' => 'required|numeric|min:1',
            'description' => 'required',
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        $frequency=Frequency::find($request->frequency_id);
        if (!isset($frequency)) {
        return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }

        $category=Category::find($request->category_id);
        if (!isset($category)) {
         return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }

        //add Success
        $kpi=new KPI();
        $kpi->startup_id=$myStartup;
        $kpi->category_id=$request->category_id;
        $kpi->frequency_id=$request->frequency_id;
        $kpi->name=$request->name;
        $kpi->target=$request->target;
        $kpi->description=$request->description;
        $kpi->format='numeric';

        $kpi->save();
        return Response()->json(['status'=>'success','Message'=>'Done Add','data'=>$kpi],200);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMyKPI(Request $request, $id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $mykpi=KPI::where('startup_id',$myStartup)->get();

        $kpi=KPI::find($id);

    foreach ($mykpi as $key => $value) {
            if ($value==$kpi) {
                $validator = Validator::make($request->all(), [
                'target' => 'numeric|min:1'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error' , 'Message' =>  $validator->errors() ] , 400);
        }

        if (isset($request->category_id)) {
            $category=Category::find($request->category_id);
            if (!isset($category)) {
            return Response()->json(['status'=>'error','Message'=>'this category doesnt exist!','data'=>''],400);
        }
           $kpi->category_id=$request->category_id;
        }
        if (isset($request->frequency_id)) {
            $frequency=Frequency::find($request->frequency_id);
            if (!isset($frequency)) {
            return Response()->json(['status'=>'error','Message'=>'this frequency doesnt exist!','data'=>''],400);
        }
           $kpi->frequency_id=$request->frequency_id;
        }
        if (isset($request->name)) {
           $kpi->name=$request->name;
        }
        if (isset($request->target)) {
           $kpi->target=$request->target;
        }
        if (isset($request->description)) {
           $kpi->description=$request->description;
        }
//        if (isset($request->format)) {
//           $kpi->format=$request->format;
//        }
        $kpi->startup_id=$myStartup;
        $kpi->save();
        return Response()->json(['status'=>'success','Message'=>'Done Update','data'=>$kpi],200);

            }
        }
      //  if (!isset($kpi)) {
        return Response()->json(['status'=>'error','Message'=>'this kpi doesnt exist!','data'=>''],400);
      //  }
        // else{
        // foreach ($mykpi as $key => $value) {
        //     if()
        // }
        //     }

    }
    public function destroyMyKpi($id)
    {
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $mykpi=KPI::where('startup_id',$myStartup)->get();

        $kpi=KPI::find($id);

        foreach ($mykpi as $key => $value) {
            if ($value==$kpi) {
        $kpi->delete();
        return Response()->json(['status'=>'success','Message'=>'Deleted it!'],200);
    }
}
       // if (!isset($kpi)) {
        return Response()->json(['status'=>'error','Message'=>'this kpi doesnt exist!','data'=>''],400);
       // }

    }
    public function findMyKPI($id){
        $currentUser=Auth::user()->id;
        $myStartup = StartupMember::where('user_id',$currentUser)->value('startup_id');
        $mykpi=KPI::where('startup_id',$myStartup)->with('datas')->get();

        $kpi=KPI::where('id',$id)->with(['datas'=> function($query)  {$query->orderBy('date', 'asc');}])->get();
        foreach ($mykpi as $key => $value) {
            if ($value->id==$id) {
            return Response()->json(['status'=>'success','Message'=>'found it!','data'=>$kpi],200);
        }
            }
        //if (!isset($kpi)) {
        return Response()->json(['status'=>'error','Message'=>'this kpi doesnt exist!','data'=>''],400);
       // }

    }
}
