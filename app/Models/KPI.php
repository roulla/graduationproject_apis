<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KPI extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function frequency()
    {
        return $this->belongsTo(Frequency::class);
    }
    public function datas()
    {
        return $this->hasMany(KPI_Data::class,'kpi_id');
    }
}
