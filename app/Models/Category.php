<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function kpis()
    {
        return $this->hasMany(KPI::class);
    }
}
