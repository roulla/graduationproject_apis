<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
	/**
     * @var string
     */
    protected $table = 'investors';

    /**
     * @var array
     */
    protected $guarded = [];

    public function investorShares()
    {
    	return $this->hasMany(InvestorStartup::class,'investor_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
}
