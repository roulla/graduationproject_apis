<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
	//protected $primaryKey = 'id';
	protected $fillable = [
         'id',
         'name',
         'balance',
         'status',
         'startup_id',
         'group_id',
         'parent_id'
    ];
    protected $table = 'accounts';
    public function startup()
    {
        return $this->belongsTo(Startup::class);
    }

    public function group()
    {
        return $this->belongsTo(AccountGroup::class);
    }
    public function creditTransactions()
    {
        return $this->hasMany(transaction::class,'credit_id');
    }
    public function debitTransactions()
    {
        return $this->hasMany(transaction::class,'debit_id');
    }
}
