<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Frequency extends Model
{
    public function kpis()
    {
        return $this->hasMany(KPI::class);
    }
}
