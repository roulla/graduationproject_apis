<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountGroup extends Model
{
    public function accounts()
    {
        return $this->hasMany(Account::class,'group_id');
    }
}
