<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvestorStartup extends Model
{
    // public function startups()
    // {
    // return $this->belongsToMany('App\Startup')->withPivot('shares');
    // }
   public function investors() 
   {
    return $this->belongsToMany('App\Models\Investor', 'InvestorStartup')->withPivot('shares');
   }

   public function startups() 
   {
    return $this->belongsToMany('App\Models\Startup', 'InvestorStartup')->withPivot('shares');
   }

}
