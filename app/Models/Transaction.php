<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function debit()
	{
    return $this->belongsTo(Account::class, 'debit_id');
	}

	public function credit()
	{
    return $this->belongsTo(Account::class, 'credit_id');
	}

	public function type()
    {
        return $this->belongsTo(TransactionType::class);
    }
}
