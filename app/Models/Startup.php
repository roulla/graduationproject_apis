<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Startup extends Model
{
    public function startupMembers()
    {
        return $this->hasMany(StartupMember::class,'startup_id');
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    // public function investors()
    // {
    // return $this->belongsToMany('App\Investor')->withPivot('shares');
    // }
    public function startupShares()
    {
    	return $this->hasMany(InvestorStartup::class,'startup_id');
    }
}
