<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KPI_Data extends Model
{
    public function kpi()
    {
        return $this->belongsTo(KPI::class);
    }
}
