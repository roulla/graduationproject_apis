<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StartupMember extends Model
{
	protected $table = 'startup_members';
	 /**
     * @var array
     */
    protected $guarded = [];

    public function startup()
    {
        return $this->belongsTo(Startup::class);
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
}
