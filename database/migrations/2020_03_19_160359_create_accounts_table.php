<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('balance');
            $table->enum('status', ['closed', 'opened'])->default('opened');

            $table->unsignedBigInteger('startup_id');
            $table->foreign('startup_id')
                ->references('id')->on('startups')
                ->onDelete('cascade');
                
            $table->unsignedBigInteger('group_id');
            $table->foreign('group_id')
                ->references('id')->on('account_groups')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
