<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStartupMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('startup_members', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->string('position');
            $table->string('address');
            $table->string('phone');
           // $table->longText('pic-path')->nullable();

            $table->unsignedBigInteger('startup_id');
            $table->foreign('startup_id')
                ->references('id')->on('startups')
                ->onDelete('cascade');

            $table->float('shares');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('startup_members');
    }
}
