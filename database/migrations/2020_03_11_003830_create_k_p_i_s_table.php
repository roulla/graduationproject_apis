<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKPISTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('k_p_i_s', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('startup_id');
            $table->foreign('startup_id')
                ->references('id')->on('startups')
                ->onDelete('cascade');

            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
            
            $table->unsignedBigInteger('frequency_id');
            $table->foreign('frequency_id')
                ->references('id')->on('frequencies')
                ->onDelete('cascade');

            $table->string('name');
            $table->integer('target');
            $table->text('description');
            $table->string('format');

            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('k_p_i_s');
    }
}
