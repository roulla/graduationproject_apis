<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->string('position');
            $table->string('address');
            $table->string('phone');
          //  $table->longText('pic-path')->nullable();

            $table->enum('type', ['startupMember', 'acceleratorMember'
                ,'externalMember'])->default('acceleratorMember');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investors');
    }
}
