<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKPIDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('k_p_i__data', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->unsignedBigInteger('kpi_id');
            $table->foreign('kpi_id')
                ->references('id')->on('k_p_i_s')
                ->onDelete('cascade');

            $table->integer('actual');
            $table->date('date');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('k_p_i__data');
    }
}
