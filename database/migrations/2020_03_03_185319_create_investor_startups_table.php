<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestorStartupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investor_startups', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('startup_id');
            $table->foreign('startup_id')
                ->references('id')->on('startups')
                ->onDelete('cascade');

            $table->unsignedBigInteger('investor_id');
            $table->foreign('investor_id')
                ->references('id')->on('investors')
                ->onDelete('cascade');

            $table->float('shares');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investor_startups');
    }
}
