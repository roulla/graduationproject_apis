<?php

use App\Models\InvestorStartup;
use Illuminate\Database\Seeder;

class InvestorStartUpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $investor = [
            [
                'shares' => '10',
                'investor_id' => '1',
                'startup_id' => '1'
            ],
            [
                'shares' => '10',
                'investor_id' => '2',
                'startup_id' => '2'
            ],
            [
                'shares' => '10',
                'investor_id' => '3',
                'startup_id' => '3'
            ],
        ];
        foreach ($investor as $key => $value) {
            InvestorStartup::create($value);
        }
    }
}
