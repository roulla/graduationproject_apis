<?php

use App\Models\StartupMember;
use Illuminate\Database\Seeder;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $member = [
            [
                'name' => 'Mhd AlHussien',
                'position' => 'Developer',
                'address' => 'Damascus',
                'phone' => '0997166694',
                'startup_id' => '1',
                'shares' => '30'
            ],
            [
                'name' => 'Moaaz Jirody',
                'position' => 'Developer',
                'address' => 'Damascus',
                'phone' => '0932451223',
                'startup_id' => '1',
                'shares' => '30'
            ],
            [
                'name' => 'Samer Orfali',
                'position' => 'Designer',
                'address' => 'Damascus',
                'phone' => '0988769431',
                'startup_id' => '1',
                'shares' => '10'
            ],
            [
                'name' => 'Yamen kh',
                'position' => 'Manager',
                'address' => 'Damascus',
                'phone' => '0997334561',
                'startup_id' => '3',
                'shares' => '50'
            ],[
                'name' => 'Ali shyah',
                'position' => 'Manager',
                'address' => 'Damascus',
                'phone' => '0992816732',
                'startup_id' => '2',
                'shares' => '30'
            ],
        ];
        foreach ($member as $key => $value) {
            StartupMember::create($value);
        }
    }
}
