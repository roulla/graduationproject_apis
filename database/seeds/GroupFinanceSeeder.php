<?php

use Illuminate\Database\Seeder;
use App\Models\AccountGroup;
class GroupFinanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = [
            [
                'name' => 'Assets',
                'description' => 'any resource owned by a business or an economic entity'
            ],
            [
                'name' => 'Expenses',
                'description' => 'money spent by Startups for work-related purposes'
            ],
            [
                'name' => 'Revenues',
                'description' => 'income that a business has from its normal business activities, usually from the sale of goods and services to customers'
            ],
        ];
        foreach ($group as $key => $value) {
            AccountGroup::create($value);
        }
    }
}
