<?php

use App\Models\KPI;
use Illuminate\Database\Seeder;

class KPISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kpi = [
            [
                'startup_id' => '3',
                'category_id' => '1',
                'frequency_id' => '2' ,
                'name' => 'The number of cards sold' ,
                'target' => '10000',
                'description' => 'you should be sell 10000 cards',
                'format' => 'numeric'
            ],
            [
                'startup_id' => '3',
                'category_id' => '2',
                'frequency_id' => '2' ,
                'name' => 'The number of application users' ,
                'target' => '10000',
                'description' => 'you should have 10000 users',
                'format' => 'numeric'
            ],
            [
                'startup_id' => '1',
                'category_id' => '2',
                'frequency_id' => '2' ,
                'name' => 'The number of application users' ,
                'target' => '10000',
                'description' => 'you should have 10000 users',
                'format' => 'numeric'
            ],
            [
                'startup_id' => '1',
                'category_id' => '1',
                'frequency_id' => '2' ,
                'name' => 'Number of reservations' ,
                'target' => '100',
                'description' => 'you should have 100 reservations',
                'format' => 'numeric'
            ],
            [
                'startup_id' => '2',
                'category_id' => '2',
                'frequency_id' => '3' ,
                'name' => 'Number of job applications' ,
                'target' => '1000',
                'description' => 'you should have 1000 jobs',
                'format' => 'numeric'
            ],
            [
                'startup_id' => '2',
                'category_id' => '2',
                'frequency_id' => '2' ,
                'name' => 'The number of application users' ,
                'target' => '10000',
                'description' => 'you should have 10000 users',
                'format' => 'numeric'
            ],

        ];
        foreach ($kpi as $key => $value) {
            KPI::create($value);
        }
    }
}
