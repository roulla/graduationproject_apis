<?php

use App\Models\AccountGroup;
use Illuminate\Database\Seeder;
use App\Models\Frequency;

class FrequencyKpiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $frequency = [
            [
                'name' => 'daily',
                'description' => 'You Can enter your information on a daily basis'
            ],
            [
                'name' => 'weekly',
                'description' => 'You Can enter your information on a weekly basis'
            ],
            [
                'name' => 'monthly',
                'description' => 'You Can enter your information on a monthly basis'
            ],
        ];
        foreach ($frequency as $key => $value) {
            Frequency::create($value);
        }
    }
}