<?php

use App\Models\Account;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account = [
            [
                'name' => 'Cap10',
                'balance' => '10000',
                'status' => 'opened',
                'startup_id' => '1',
                'group_id' => '1',
                'parent_id' => null
            ],
            [
                'name' => 'Forsa',
                'balance' => '10000',
                'status' => 'opened',
                'startup_id' => '2',
                'group_id' => '1',
                'parent_id' => null
            ],
            [
                'name' => 'Tio',
                'balance' => '10000',
                'status' => 'opened',
                'startup_id' => '3',
                'group_id' => '1',
                'parent_id' => null
            ],
        ];
        foreach ($account as $key => $value) {
            Account::create($value);
        }
    }
}
