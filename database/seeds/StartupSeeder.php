<?php

use App\Models\Startup;
use Illuminate\Database\Seeder;

class StartupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startup = [
            [
                'name' => 'Cap10',
                'phase' => 2,
                'joinDate' => '2019-05-11'
            ],
            [
                'name' => 'Forsa',
                'phase' => 3,
                'joinDate' => '2019-06-15'
            ],
            [
                'name' => 'Tio',
                'phase' => 2,
                'joinDate' => '2019-06-22'
            ],
        ];
        foreach ($startup as $key => $value) {
            Startup::create($value);
        }
    }
}
