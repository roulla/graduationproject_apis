<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'samer',
                'email' => 'samer@gmail.com',
                'password' => Hash::make('password')
            ],
            [
                'name' => 'mohammad',
                'email' => 'mohammad@gmail.com',
                'password' => Hash::make('password')
            ],
            [
                'name' => 'ali',
                'email' => 'ali@gmail.com',
                'password' => Hash::make('password')
            ],
            [
                'name' => 'yamen',
                'email' => 'yamen@gmail.com',
                'password' => Hash::make('password')
            ],
        ];
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}