<?php

use App\Models\Investor;
use Illuminate\Database\Seeder;

class InvestorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $investor = [
            [
                'name' => 'Dr.Azzat Alsous',
                'position' => 'Manager',
                'address' => 'Damascus',
                'phone' => '0997166694',
                'type' => 'acceleratorMember'
            ],
            [
                'name' => 'Mr.Ali Shaker',
                'position' => 'Manager',
                'address' => 'Damascus',
                'phone' => '0997198941',
                'type' => 'acceleratorMember'
            ],
            [
                'name' => 'Saeb Nahas',
                'position' => 'Manager',
                'address' => 'Damascus',
                'phone' => '0997162238',
                'type' => 'acceleratorMember'
            ],
        ];
        foreach ($investor as $key => $value) {
            Investor::create($value);
        }
    }
}
