<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->delete();

        //1) Create Accelerator Role
        $role = ['name' => 'Accelerator', 'display_name' => 'Accelerator', 'description' => 'Full Permission'];
        $role = Role::create($role);


        $role2 = ['name' => 'Startup', 'display_name' => 'Startup', 'description' => 'some Permission'];
        $role2 = Role::create($role2);


        //2) Set Role Permissions
        // Get all permission, swift through and attach them to the role
        $permission = Permission::get();
        foreach ($permission as $key => $value) {
            $role->attachPermission($value);
        }

        //3) Create Accelerator User
        $user = ['name' => 'Dr.Azzat AlSous', 'email' => 'azzat.sous@gmail.com', 'password' => Hash::make('adminpassword')];
        $user = User::create($user);


        //4) Set User Role
        $user->attachRole($role);
    }
}
