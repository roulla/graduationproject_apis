<?php

use App\Models\TransactionType;
use Illuminate\Database\Seeder;

class TypeTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = [
            [
                'name' => 'External',
                'description' => 'External transaction is a type of transaction that occur between two persons or two organizations or between a person and organization in terms of money.'
            ],
            [
                'name' => 'Internal',
                'description' => 'Internal transaction is an economic activity which occurs within a company or a business organization. It is an exchange from one department to another in the same company.'
            ],
            [
                'name' => 'Cash',
                'description' => 'This is a type of transactions which are settled for cash right after their occurrence. Cash transactions means exchanging goods for cash. For example, Mr. Kelvin sold an electric iron for cash for use.'
            ],
            [
                'name' => 'Non-Cash',
                'description' => 'Any transactions which are not cash transactions and credit transactions are collectively known as non-cash transactions. Examples of non-cash transactions are: depreciation of fixed assets, the return of defective goods purchased earlier etc.'
            ],
        ];
        foreach ($type as $key => $value) {
            TransactionType::create($value);
        }
    }
}