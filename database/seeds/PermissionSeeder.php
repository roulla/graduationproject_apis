<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            [
                'name' => 'role-create',
                'display_name' => 'Create Role',
                'description' => 'Create New Role'
            ],
            [
                'name' => 'role-list',
                'display_name' => 'Display Role Listing',
                'description' => 'List All Roles'
            ],
            [
                'name' => 'role-update',
                'display_name' => 'Update Role',
                'description' => 'Update Role Information'
            ],
            [
                'name' => 'role-delete',
                'display_name' => 'Delete Role',
                'description' => 'Delete Role'
            ],
            [
                'name' => 'user-create',
                'display_name' => 'Create User',
                'description' => 'Create New User'
            ],
            [
                'name' => 'user-list',
                'display_name' => 'Display User Listing',
                'description' => 'List All Users'
            ],
            [
                'name' => 'user-update',
                'display_name' => 'Update User',
                'description' => 'Update User Information'
            ],
            [
                'name' => 'user-delete',
                'display_name' => 'Delete User',
                'description' => 'Delete User'
            ],
            [
                'name' => 'investor-create',
                'display_name' => 'Create Investor',
                'description' => 'Create New Investor'
            ],
            [
                'name' => 'investor-list',
                'display_name' => 'Display Investor Listing',
                'description' => 'List All Investor'
            ],
            [
                'name' => 'investor-update',
                'display_name' => 'Update Investor',
                'description' => 'Update Investor Information'
            ],
            [
                'name' => 'investor-delete',
                'display_name' => 'Delete Investor',
                'description' => 'Delete Investor Information'
            ],
            [
                'name' => 'investor-find',
                'display_name' => 'Find Investor',
                'description' => 'Find Investor'
            ],
            [
                'name' => 'startup-create',
                'display_name' => 'Create Startup',
                'description' => 'Create New Startup'
            ],
            [
                'name' => 'startup-list',
                'display_name' => 'Display Startup Listing',
                'description' => 'List All Startup'
            ],
            [
                'name' => 'startup-update',
                'display_name' => 'Update Startup',
                'description' => 'Update Startup Information'
            ],
            [
                'name' => 'startup-delete',
                'display_name' => 'Delete Startup',
                'description' => 'Delete Startup Information'
            ],
            [
                'name' => 'startup-find',
                'display_name' => 'Find Startup',
                'description' => 'Find Startup'
            ],
            [
                'name' => 'startupMember-create',
                'display_name' => 'Create Startup Member',
                'description' => 'Create New Startup Member'
            ],
            [
                'name' => 'startupMember-list',
                'display_name' => 'Display Startup Member Listing',
                'description' => 'List All Startup Member'
            ],
            [
                'name' => 'startupMember-update',
                'display_name' => 'Update Startup Member',
                'description' => 'Update Startup Member Information'
            ],
            [
                'name' => 'startupMember-delete',
                'display_name' => 'Delete Startup Member',
                'description' => 'Delete Startup Member Information'
            ],
            [
                'name' => 'startupMember-find',
                'display_name' => 'Find Startup Member',
                'description' => 'Find Startup Member'
            ],
            [
                'name' => 'shares-list',
                'display_name' => 'Display shares Listing',
                'description' => 'List All shares'
            ],
            [
                'name' => 'share-update',
                'display_name' => 'Update share',
                'description' => 'Update share Information'
            ],
            [
                'name' => 'share-delete',
                'display_name' => 'Delete share',
                'description' => 'Delete share Information'
            ],
            [
                'name' => 'share-create',
                'display_name' => 'Create share',
                'description' => 'Create New Share'
            ],
            [
                'name' => 'share-find',
                'display_name' => 'Find share',
                'description' => 'Find Share'
            ],
            [
                'name' => 'KPICategory-list',
                'display_name' => 'Display KPI Category Listing',
                'description' => 'List All KPI Category'
            ],
            [
                'name' => 'KPICategory-update',
                'display_name' => 'Update KPI Category',
                'description' => 'Update KPI Category Information'
            ],
            [
                'name' => 'KPICategory-delete',
                'display_name' => 'Delete KPI Category',
                'description' => 'Delete KPI Category Information'
            ],
            [
                'name' => 'KPICategory-create',
                'display_name' => 'Create KPI Category',
                'description' => 'Create New KPI Category'
            ],
            [
                'name' => 'KPICategory-find',
                'display_name' => 'Find KPI Category',
                'description' => 'Find KPICategory'
            ],
            [
                'name' => 'KPIFrequency-list',
                'display_name' => 'Display KPI Frequency Listing',
                'description' => 'List All KPI Frequency'
            ],
            [
                'name' => 'KPIFrequency-update',
                'display_name' => 'Update KPI Frequency',
                'description' => 'Update KPI Frequency Information'
            ],
            [
                'name' => 'KPIFrequency-delete',
                'display_name' => 'Delete KPI Frequency',
                'description' => 'Delete KPI Frequency Information'
            ],
            [
                'name' => 'KPIFrequency-create',
                'display_name' => 'Create KPI Frequency',
                'description' => 'Create New KPI Frequency'
            ],
            [
                'name' => 'KPIFrequency-find',
                'display_name' => 'Find KPI Frequency',
                'description' => 'Find KPI Frequency'
            ],
            [
                'name' => 'KPI-create',
                'display_name' => 'Create KPI',
                'description' => 'Create New KPI'
            ],
            [
                'name' => 'KPI-list',
                'display_name' => 'Display KPI Listing',
                'description' => 'List All KPI'
            ],
            [
                'name' => 'KPI-update',
                'display_name' => 'Update KPI',
                'description' => 'Update KPI Information'
            ],
            [
                'name' => 'KPI-delete',
                'display_name' => 'Delete KPI',
                'description' => 'Delete KPI'
            ],
            [
                'name' => 'KPI-find',
                'display_name' => 'Find KPI',
                'description' => 'Find KPI'
            ],
            [
                'name' => 'DataKPI-create',
                'display_name' => 'Create Data KPI',
                'description' => 'Create New Data KPI'
            ],
            [
                'name' => 'DataKPI-list',
                'display_name' => 'Display Data KPI Listing',
                'description' => 'List All Data KPI'
            ],
            [
                'name' => 'DataKPI-update',
                'display_name' => 'Update Data KPI',
                'description' => 'Update Data KPI Information'
            ],
            [
                'name' => 'DataKPI-delete',
                'display_name' => 'Delete Data KPI',
                'description' => 'Delete Data KPI'
            ],
            [
                'name' => 'DataKPI-find',
                'display_name' => 'Find Data KPI',
                'description' => 'Find Data KPI'
            ],
            [
                'name' => 'accountGroup-create',
                'display_name' => 'Create Account Group',
                'description' => 'Create New Account Group'
            ],
            [
                'name' => 'accountGroup-list',
                'display_name' => 'Display Account Group Listing',
                'description' => 'List All Account Group'
            ],
            [
                'name' => 'accountGroup-update',
                'display_name' => 'Update Account Group',
                'description' => 'Update Account Group Information'
            ],
            [
                'name' => 'accountGroup-delete',
                'display_name' => 'Delete Account Group',
                'description' => 'Delete Account Group Information'
            ],
            [
                'name' => 'accountGroup-find',
                'display_name' => 'Find Account Group',
                'description' => 'Find Account Group'
            ],
            [
                'name' => 'transactionType-create',
                'display_name' => 'Create Transaction Type',
                'description' => 'Create New Transaction Type'
            ],
            [
                'name' => 'transactionType-list',
                'display_name' => 'Display Transaction Type Listing',
                'description' => 'List All Transaction Type'
            ],
            [
                'name' => 'transactionType-update',
                'display_name' => 'Update Transaction Type',
                'description' => 'Update Transaction Type Information'
            ],
            [
                'name' => 'transactionType-delete',
                'display_name' => 'Delete Transaction Type',
                'description' => 'Delete Transaction Type Information'
            ],
            [
                'name' => 'transactionType-find',
                'display_name' => 'Find Transaction Type',
                'description' => 'Find Transaction Type'
            ],
            [
                'name' => 'account-open',
                'display_name' => 'Open Account',
                'description' => 'Open New Account'
            ],
            [
                'name' => 'account-list',
                'display_name' => 'Display Accounts Listing',
                'description' => 'List All Accounts'
            ],
            [
                'name' => 'account-update',
                'display_name' => 'Update Account',
                'description' => 'Update Account Information'
            ],
            [
                'name' => 'account-close',
                'display_name' => 'Close Account',
                'description' => 'Close Account Information'
            ],
            [
                'name' => 'account-find',
                'display_name' => 'Find Account',
                'description' => 'Find Account'
            ],
            [
                'name' => 'review-Debit-Transactions',
                'display_name' => 'Review All Debit Transactions',
                'description' => 'Review All Debit Transactions'
            ],
            [
                'name' => 'review-credit-Transactions',
                'display_name' => 'Review All Credit Transactions',
                'description' => 'Review All Credit Transactions'
            ],
            [
                'name' => 'transactions-list',
                'display_name' => 'Display Transactions Listing',
                'description' => 'List All Transactions'
            ],
            [
                'name' => 'transaction-create',
                'display_name' => 'Create Transaction',
                'description' => 'Create New Transaction'
            ],
            [
                'name' => 'transaction-find',
                'display_name' => 'Find Transaction',
                'description' => 'Find Transaction'
            ]
        ];

        foreach ($permission as $key => $value) {
            Permission::create($value);
        }
    }
}
