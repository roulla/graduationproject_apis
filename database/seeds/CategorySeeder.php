<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            [
                'name' => 'Financial',
                'description' => 'used in performance measurement and when looking at an operating index.'
            ],
            [
                'name' => 'Customers',
                'description' => 'are sufficiently in an organization’s control to effect change.'
            ],
        ];
        foreach ($category as $key => $value) {
            Category::create($value);
        }
    }
}
