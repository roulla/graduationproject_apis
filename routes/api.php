<?php

use Illuminate\Http\Request;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE,PATCH');
header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization,Accept,X-Requested-With');



Route::post('login', 'APIController@login');
Route::post('register', 'APIController@register');


Route::group(['middleware' => ['auth.jwt']], function() {
    Route::get('logout', 'APIController@logout');
});
 

//Route::namespace('Admin')->group(['middleware' => ['auth.jwt','role:Accelerator']], function() {
   // Route::get('logout', 'ApiController@logout');
Route::namespace('Admin')->group(function() {
    Route::group(['middleware' => ['auth.jwt','role:Accelerator']], function() {


        //Crud Startups
	    Route::get('/startup/all','StartupController@index');
	    Route::post('/startup/add','StartupController@store');
	    Route::post('/startup/update/{id}','StartupController@update');
	    Route::get('/startup/findById/{id}','StartupController@show');
	    Route::post('/startup/delete/{id}','StartupController@destroy');
	    Route::get('/startup/getMyMembers/{id}','StartupController@getMyMembers');


	    //Crud Investor
	    Route::get('/investor/all','InvestorController@index');
	    Route::post('/investor/add','InvestorController@store');
	    Route::get('/investor/findById/{id}','InvestorController@show');
	    Route::post('/investor/update/{id}','InvestorController@update');
	    Route::post('/investor/delete/{id}','InvestorController@destroy');
        ////flatter
        Route::get('/investor/findById2/{id}','InvestorController@show2');

	    //crud Member startup
	    Route::get('/startupMember/all','StartupMemberController@index');
	    Route::post('/startupMember/add','StartupMemberController@store');
	    Route::get('/startupMember/findById/{id}','StartupMemberController@show');
	    Route::post('/startupMember/update/{id}','StartupMemberController@update');
	    Route::post('/startupMember/delete/{id}','StartupMemberController@destroy');
	    Route::post('/startupMember/addMemberByStartupId/{id}'
		,'StartupMemberController@addMemberByStartupId');
        Route::get('/startupMember/users','StartupMemberController@getUsres');
        Route::get('/startupMember/getMemberByStartupid/{id}'
        ,'StartupMemberController@getMemberByStartupId');

        //userId
        Route::get('/userRole/all','RoleController@all');
        Route::post('/userRole/delete/{id}','RoleController@destroy');

	    //crud shares
	    Route::get('/shares/all','InvestorStartupController@index');
	    Route::post('/shares/add','InvestorStartupController@store');
	    Route::get('/shares/findById/{id}','InvestorStartupController@show');
	    Route::post('/shares/update/{id}','InvestorStartupController@update');
	    Route::post('/shares/delete/{id}','InvestorStartupController@destroy');

	    //crud Category
	    Route::get('/category/all','CategoryController@index');
	    Route::post('/category/add','CategoryController@store');
	    Route::get('/category/findById/{id}','CategoryController@show');
	    Route::post('/category/update/{id}','CategoryController@update');
	    Route::post('/category/delete/{id}','CategoryController@destroy');

	    //crud Frequency
	    Route::get('/frequency/all','FrequencyController@index');
	    Route::post('/frequency/add','FrequencyController@store');
	    Route::get('/frequency/findById/{id}','FrequencyController@show');
	    Route::post('/frequency/update/{id}','FrequencyController@update');
	    Route::post('/frequency/delete/{id}','FrequencyController@destroy');

	    //crud KPI
	    Route::get('/kpi/all','KPIController@index');
	    Route::post('/kpi/add','KPIController@store');
	    Route::get('/kpi/findById/{id}','KPIController@show');
	    Route::post('/kpi/update/{id}','KPIController@update');
	    Route::post('/kpi/delete/{id}','KPIController@destroy');

	    //crud KPI data
	    Route::get('/kpiData/all','KPIDataController@index');
	    Route::post('/kpiData/add','KPIDataController@store');
	    Route::get('/kpiData/findById/{id}','KPIDataController@show');
	    Route::post('/kpiData/update/{id}','KPIDataController@update');
	    Route::post('/kpiData/delete/{id}','KPIDataController@destroy');
        Route::get('/kpiData/getTotal/{kpiId}','KPIDataController@getTotal');
        Route::get('/kpi/{id}/find/data','KPIDataController@findByKpiId');


        //crud Account Group
	    Route::get('/accountGroup/all','AccountGroupController@index');
	    Route::post('/accountGroup/add','AccountGroupController@store');
	    Route::get('/accountGroup/findById/{id}','AccountGroupController@show');
	    Route::post('/accountGroup/update/{id}','AccountGroupController@update');
	    Route::post('/accountGroup/delete/{id}','AccountGroupController@destroy');

	    //crud Transaction Type
	    Route::get('/transactionType/all','TransactionTypeController@index');
	    Route::post('/transactionType/add','TransactionTypeController@store');
	    Route::get('/transactionType/findById/{id}','TransactionTypeController@show');
	    Route::post('/transactionType/update/{id}','TransactionTypeController@update');
	    Route::post('/transactionType/delete/{id}','TransactionTypeController@destroy');

	    //Manage Accounts
	    Route::get('/account/all/{status}','AccountController@index');
	    Route::post('/account/open','AccountController@store');
	    Route::get('/account/findById/{id}','AccountController@show');
	    Route::post('/account/update/{id}','AccountController@update');
	    Route::post('/account/close/{id}','AccountController@close');
	    //Route::get('/Account/getAllClosedAccounts','AccountController@GetAllClosedAccounts');
	    //Route::get('/Account/getBalance/{id}','AccountController@GetBalance');
	    Route::get('/account/reviewAllDebitTransactions/{id}',
	    'AccountController@ReviewAllDebitTransactions');
	    Route::get('/account/reviewAllCreditTransactions/{id}',
	    'AccountController@ReviewAllCreditTransactions');

	    //Make Transfers
	    Route::get('/transaction/all','TransactionController@index');
	    Route::post('/transaction/add','TransactionController@store');
	    Route::get('/transaction/findById/{id}','TransactionController@show');
        Route::get('/transaction/mytrans/{id}','TransactionController@GetAccounttrans');


        ///flatter
        Route::get('/shares/getMyinvestor/{id}','InvestorStartupController@getMyinvestor');
        Route::post('/shares/addinvestorByStartupId/{id}','InvestorStartupController@addinvestorByStartupId');
        Route::post('/kpi/addkpiByStartupId/{id}','KPIController@addkpiByStartupId');
        Route::get('/kpi/getMykpiByStartupId/{id}','KPIController@getMykpi');
        Route::get('/category/getMycategory/{id}','KPIController@getMycategory');
        Route::get('/frequency/getMyfrequency/{id}','KPIController@getMyfrequency');
        Route::get('/account/getMyaccountsByStartupId/{id}','AccountController@getMyaccountsByStartupId');

        ///search
        Route::get('/startupMember/search/{name}','StartupMemberController@search');
        Route::get('/startup/search/{name}','StartupController@search');
        Route::get('/category/search/{name}','CategoryController@search');
        Route::get('/kpi/search/{name}','KPIController@search');
        Route::get('/accountGroup/search/{name}','AccountGroupController@search');
        Route::get('/transaction/search/{name}','TransactionController@search');


        //Redmine

        //Projects
        Route::get('/redmine/all/project/{startUpname}','RedmineProjectController@all');
        Route::get('/redmine/all/project','RedmineProjectController@all2');
        Route::get('/redmine/show/project/{projectId}','RedmineProjectController@show');
        Route::post('/redmine/add/project','RedmineProjectController@add');
        Route::post('/redmine/update/project/{projectId}','RedmineProjectController@update');
        Route::post('/redmine/delete/project/{projectId}','RedmineProjectController@delete');
        //Route::get('/redmine/project/getIdByName/{name}','RedmineProjectController@getIdByName');

        //Issues
        Route::get('/redmine/all/priority/issue','IssueController@allIssuePriority');
        Route::get('/redmine/all/status/issue','IssueController@allIssueStatus');
        Route::get('/redmine/show/status/{name}','IssueController@getIdStatusByName');
        Route::get('/redmine/all/issue/{id}','IssueController@all');
        Route::get('/redmine/issue/{id}/close','IssueController@closedIssue');
        Route::post('/redmine/issue/add','IssueController@store');
        Route::get('/redmine/issue/show/{id}','IssueController@show');
        Route::post('/redmine/issue/update/{id}','IssueController@update');
        Route::post('/redmine/issue/delete/{id}','IssueController@destroy');
        Route::get('/redmine/all/closed/issue/{project_id}','IssueController@all_closed');
        Route::post('/redmine/issue/attach/upload/{id}','IssueController@attach');
        Route::get('/redmine/issue/attach/download/{id}','IssueController@download');
        
        //calendar
        Route::post('/redmine/calendar/add/{startupName}','IssueController@pickDate');
        Route::get('/redmine/calendar/get/{startupName}','IssueController@get_pickDate');

        ////wiki
        Route::get('/admin/redmine/wiki/all/{project}','WikiController@all');
        Route::get('/admin/redmine/wiki/all','WikiController@all2');
        Route::post('/admin/redmine/wiki/show/{title}','WikiController@show');
        Route::post('/admin/redmine/wiki/update/{title}','WikiController@update');
        Route::post('/admin/redmine/wiki/delete/{title}','WikiController@delete');
        Route::post('/admin/redmine/wiki/download/{id}','WikiController@download');
        Route::post('/admin/redmine/wiki/download2','WikiController@downloadFromWiki');
        Route::post('/admin/redmine/wiki/upload','WikiController@upload');
        Route::post('/admin/redmine/wiki/upload2','WikiController@upload2');


        //Users
        Route::get('/users/all','UserController@index');
        Route::post('/users/add','UserController@store');
        Route::get('/users/findById/{id}','UserController@show');
        Route::post('/users/update/{id}','UserController@update');
        Route::post('/users/delete/{id}','UserController@destroy');

    });

});

//Route::namespace('Startup')->group(['middleware' => ['auth.jwt','role:Startup']], function() {
Route::namespace('Startup')->group(function() {
    Route::group(['middleware' => ['auth.jwt','role:Startup']], function() {

        //startup Operations
	    Route::get('/startup/retrieve','StartupController@index');
	    Route::get('/startup/find/{id}','StartupController@show');

	    //kpi operations
	    Route::get('/startup/kpi/all','KPIController@getMyKPIs');
	    Route::post('/startup/kpi/add','KPIController@addMyKPI');
	    Route::post('/startup/kpi/update/{id}','KPIController@updateMyKPI');
	    Route::post('/startup/kpi/delete/{id}','KPIController@destroyMyKpi');
	    Route::get('/startup/kpi/find/{id}','KPIController@findMyKPI');

        //crud Member startup
        Route::get('/startup/member/all','StartupMemberController@index');
        Route::post('/startup/member/add','StartupMemberController@store');
        Route::get('/startup/member/findById/{id}','StartupMemberController@show');
        Route::post('/startup/member/update/{id}','StartupMemberController@update');
        Route::post('/startup/member/delete/{id}','StartupMemberController@destroy');
        Route::get('/startup/member/users','StartupMemberController@getUsres');

        //Category operations
        Route::get('/startup/category/all','CategoryController@index');
        Route::post('/startup/category/add','CategoryController@store');
        Route::get('/startup/category/findById/{id}','CategoryController@show');


        //frequency operations
        Route::get('/startup/frequency/all','FrequencyController@index');
        Route::get('/startup/frequency/findById/{id}','FrequencyController@show');


        //crud KPI data
        Route::get('/startup/kpiData/all','KPIDataController@index');
        Route::post('/startup/kpiData/add','KPIDataController@store');
        Route::get('/startup/kpiData/findById/{id}','KPIDataController@show');
        Route::post('/startup/kpiData/update/{id}','KPIDataController@update');
        Route::post('/startup/kpiData/delete/{id}','KPIDataController@destroy');
        Route::get('/startup/kpiData/getTotal/{kpiId}','KPIDataController@getTotal');
        


        //financial Operations

        //crud Account Group
        Route::get('/startup/accountGroup/all','AccountGroupController@index');
        Route::post('/startup/accountGroup/add','AccountGroupController@store');
        Route::get('/startup/accountGroup/findById/{id}','AccountGroupController@show');

        //crud Transaction Type
        Route::get('/startup/transactionType/all','TransactionTypeController@index');
        Route::post('/startup/transactionType/add','TransactionTypeController@store');
        Route::get('/startup/transactionType/findById/{id}','TransactionTypeController@show');

        //Manage Accounts
        Route::get('/startup/account/all/{status}','AccountController@index');
        Route::post('/startup/account/open','AccountController@store');
        Route::get('/startup/account/findById/{id}','AccountController@show');
        Route::post('/startup/account/update/{id}','AccountController@update');
        Route::post('/startup/account/close/{id}','AccountController@close');

        Route::get('/startup/account/reviewAllDebitTransactions/{id}',
            'AccountController@ReviewAllDebitTransactions');
        Route::get('/startup/account/reviewAllCreditTransactions/{id}',
            'AccountController@ReviewAllCreditTransactions');

        //Make Transfers
        Route::get('/startup/transaction/all','TransactionController@index');
        Route::post('/startup/transaction/add','TransactionController@store');
        Route::get('/startup/transaction/findById/{id}','TransactionController@show');
        ////
        Route::get('/startup/transaction/mytrans/{id}','TransactionController@GetAccounttrans');


        /////redmine
        /// project
        Route::get('/redmine/project/all','IssueController@allproject');
        ///issue
        Route::get('/redmine/issues/all/{project}','IssueController@allissue');
        Route::get('/redmine/issues/all/closed/{project}','IssueController@closedIssue');
        Route::get('/redmine/issues/findById/{id}','IssueController@showissue');
        Route::post('/redmine/issues/add/{project}','IssueController@createissue');
        Route::post('/redmine/issues/update/{id}','IssueController@updateissue');
        Route::post('/redmine/issues/delete/{id}','IssueController@deleteissue');
        ///status
        Route::get('/redmine/status/all','IssueController@allstatus');
        Route::get('/redmine/status/getbyname/{name}','IssueController@getissueStatusesIdByName');
        ///Priorities
        Route::get('/redmine/priorities/all','IssueController@allPriorities');
        Route::get('/redmine/priorities/getbyname/{name}','IssueController@getPrioritiesByName');
        ///attach
        Route::post('/redmine/issues/attach/upload/{id}','IssueController@attach');
        Route::get('/redmine/issues/attach/download/{id}','IssueController@download');

        //calendar
        Route::post('/redmine/calendar/add','IssueController@pickDate');
        Route::get('/redmine/calendar/get','IssueController@get_pickDate');

        ////wiki
        Route::get('/redmine/wiki/all/{project}','WikiController@all');
        Route::post('/redmine/wiki/show/{title}','WikiController@show');
        Route::post('/redmine/wiki/update/{title}','WikiController@update');
        Route::post('/redmine/wiki/delete/{title}','WikiController@delete');
        Route::post('/redmine/wiki/download/{id}','WikiController@download');
        Route::post('/redmine/wiki/download2/{id}','WikiController@downloadFromWiki');
        Route::post('/redmine/wiki/upload','WikiController@upload');

        ////search
        Route::get('/redmine/search/{string}','SearchController@search');

    });
});


